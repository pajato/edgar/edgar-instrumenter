package com.pajato.edgar.collector

import com.pajato.isaac.api.DocumentSymbol

internal fun getRecordText() = "com.pajato.edgar.collector.DataCollector.record(${DataCollector.getNextId()})"

object DataCollector {

    internal val dataMap: MutableMap<Int, ExecutableSnippet> = mutableMapOf()

    fun addExecutableSnippet(id: Int, symbol: DocumentSymbol) { dataMap[id] = ExecutableSnippet(id, symbol) }

    fun clear() {
        dataMap.clear()
    }

    fun getNextId() = dataMap.size

    fun <T> record(id: Int, script: () -> T): T {
        fun incrementCount(snippet: ExecutableSnippet) { snippet.executionCount++ }
        fun logMissingSnippet() { println("No information for id: $id in the coverage data map!") }

        when (val entry = dataMap[id]) {
            null -> logMissingSnippet()
            else -> incrementCount(entry)
        }
        return script()
    }
}

data class ExecutableSnippet(val id: Int, val symbol: DocumentSymbol, var executionCount: Int = 0)
