package com.pajato.edgar.instrumenter

import com.pajato.edgar.collector.getRecordText
import com.pajato.edgar.core.Diagnostic

internal typealias ArgList = MutableList<String>

internal const val arrayAccessExpressionDetail = "KtArrayAccessExpression"
internal const val binaryExpressionDetail = "KtBinaryExpression"
internal const val containerNodeDetail = "KtContainerNode"
internal const val operationReferenceExpressionDetail = "KtOperationReferenceExpression"

internal fun Symbol.getDiagnosticOrRegisterBinaryExpression(
    editor: Editor,
    list: ArgList = mutableListOf()
): Diagnostic = when (children) {
    null -> getNullListMessage()
    else -> children!!.getDiagnosticOrRegisterBinaryExpressionChildren(editor, list).ifEmpty {
        getDiagnosticOrRegisterWithText(editor, list.joinToString("", "", ""))
    }
}

internal fun Children.getDiagnosticOrRegisterBinaryExpressionChildren(editor: Editor, list: ArgList): Diagnostic =
    when {
        size != 3 -> getInvalidSizeMessage(size, 3)
        else -> this[0].getDiagnosticOrRegisterLeftHandSide(editor, list).ifEmpty {
            this[1].getDiagnosticOrRegisterOperation(editor, list).ifEmpty {
                this[2].getDiagnosticOrRegisterRightHandSide(editor, list)
            }
        }
    }

internal fun Symbol.getDiagnosticOrRegisterLeftHandSide(editor: Editor, list: ArgList): Diagnostic = when (detail) {
    nameReferenceExpressionDetail -> getDiagnosticOrRegisterNameReferenceExpression(editor, list)
    arrayAccessExpressionDetail -> getDiagnosticOrRegisterArrayAccessExpression(editor, list)
    else -> getUnsupportedDetailMessage(detail, arrayAccessExpressionDetail)
}

internal fun Symbol.getDiagnosticOrRegisterOperation(editor: Editor, list: ArgList): Diagnostic = when (detail) {
    operationReferenceExpressionDetail -> getDiagnosticOrRegisterOperationReferenceExpression(editor, list)
    else -> getUnsupportedDetailMessage(detail, operationReferenceExpressionDetail)
}

internal val rightHandSideChoices: Set<String> = setOf(
    constantExpressionDetail, nameReferenceExpressionDetail, callExpressionDetail
)

internal fun Symbol.getDiagnosticOrRegisterRightHandSide(editor: Editor, list: ArgList): Diagnostic = when {
    rightHandSideChoices.contains(detail) -> getDiagnosticOrRegisterSelection(editor, list)
    else -> getUnsupportedDetailMessage(detail, rightHandSideChoices)
}

internal fun Symbol.getDiagnosticOrRegisterSelection(editor: Editor, list: ArgList): Diagnostic {
    list.add(" ${getRecordText()} { ${getSelectionText(editor)} }")
    return ""
}

internal fun Symbol.getDiagnosticOrRegisterArrayAccessExpression(editor: Editor, list: ArgList): Diagnostic =
    children?.getDiagnosticOrRegisterArrayAccessExpressionChildren(editor, list) ?: getNullListMessage()

internal fun Children.getDiagnosticOrRegisterArrayAccessExpressionChildren(editor: Editor, list: ArgList): Diagnostic =
    if (size != 2) getInvalidSizeMessage(size, 2) else
        this[0].getDiagnosticOrRegisterNameReferenceExpression(editor, list).ifEmpty {
            list.add("[")
            this[1].getDiagnosticOrRegisterContainerNode(editor, list).ifEmpty { list.add(" ] "); "" }
        }

internal fun Symbol.getDiagnosticOrRegisterNameReferenceExpression(editor: Editor, list: ArgList): Diagnostic =
    when (detail) {
        nameReferenceExpressionDetail -> getDiagnosticOrRegisterSymbolOnList(editor, list)
        else -> getUnsupportedDetailMessage(detail, nameReferenceExpressionDetail)
    }

internal fun Symbol.getDiagnosticOrRegisterSymbolOnList(editor: Editor, list: ArgList): Diagnostic = when {
    list.add(editor.getText(range.start, range.end)) ->
        editor.getDiagnosticOrRegisterInitialCountValue(this)
    else -> getListRegisterErrorMessage(detail)
}

internal fun Symbol.getDiagnosticOrRegisterContainerNode(editor: Editor, list: ArgList): Diagnostic = when {
    children == null -> getNullListMessage()
    children!!.size != 1 -> getInvalidSizeMessage(children!!.size, 1)
    detail == containerNodeDetail -> children!![0].getDiagnosticOrRegisterContainerNodeChild(editor, list)
    else -> getUnsupportedDetailMessage(detail, containerNodeDetail)
}

internal val containerNodeChildChoices = setOf(constantExpressionDetail, nameReferenceExpressionDetail)

internal fun Symbol.getDiagnosticOrRegisterContainerNodeChild(editor: Editor, list: ArgList): Diagnostic = when {
    containerNodeChildChoices.contains(detail) -> getDiagnosticOrRegisterSelection(editor, list)
    else -> getUnsupportedDetailMessage(detail, containerNodeChildChoices)
}

internal fun Symbol.getDiagnosticOrRegisterOperationReferenceExpression(editor: Editor, list: ArgList): Diagnostic =
    this.getDiagnosticOrRegisterSymbolOnList(editor, list)

// Error message functions

internal fun getListRegisterErrorMessage(detail: String?): Diagnostic =
    "Unable to register name from symbol with detail: $detail tp list!"
