package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import java.io.File

internal val registeredEditList: MutableList<EditInfo> = mutableListOf()
internal val executionCountMap: MutableMap<DocumentSymbol, Int> = mutableMapOf()

internal class FileEditor(override val file: File) : Editor {

    override var text: String = file.readText()
    private val lineNumberIndexMap: Map<Int, Int> = getLineNumberIndexes(text)

    init {
        registeredEditList.clear()
        executionCountMap.clear()
    }

    override fun getText(start: Position, end: Position): String {
        fun isValid(startIndex: Int, endIndex: Int) = startIndex != -1 && endIndex != -1
        fun getFlatText(start: Int, end: Int) = text.substring(start, end).replace("\n", "\\n")

        val startIndex = getIndex(start)
        val endIndex = getIndex(end)
        return if (isValid(startIndex, endIndex)) getFlatText(getIndex(start), getIndex(end)) else ""
    }

    private fun getDiagnosticOrReplaceText(info: EditInfo) = getDiagnosticOrReplaceText(
        info.replacementText,
        info.symbol.range.start,
        info.symbol.range.end,
    )

    override fun getDiagnosticOrReplaceText(replacement: String, start: Position, end: Position): Diagnostic {
        val startIndex = convert(start)
        val endIndex = convert(end)

        fun getReplacedText(startIndex: Int, endIndex: Int): String {
            val startText = text.substring(0, startIndex)
            val endText = text.substring(endIndex, text.length)
            return startText + replacement + endText
        }

        if (startIndex == -1) return "Invalid start index detected!"
        if (endIndex == -1) return "Invalid end index detected!"
        text = getReplacedText(startIndex, endIndex)
        return ""
    }

    override fun getIndex(position: Position): Int {
        val lineIndex = lineNumberIndexMap[position.line] ?: -1
        return if (lineIndex != -1) lineIndex + position.character else lineIndex
    }

    override fun getDiagnosticOrUpdate(): Diagnostic {
        file.writeText(text)
        return ""
    }

    override fun convert(position: Position): Int = lineNumberIndexMap[position.line]?.plus(position.character) ?: -1

    override fun getDiagnosticOrRegisterInitialCountValue(symbol: DocumentSymbol): Diagnostic {
        executionCountMap[symbol] = 0
        return ""
    }

    override fun getDiagnosticOrRegisterSymbol(symbol: DocumentSymbol, replacementText: String): Diagnostic {
        registeredEditList.add(EditInfo(symbol, replacementText))
        return getDiagnosticOrRegisterInitialCountValue(symbol)
    }

    override fun getDiagnosticOrPerformEdits(): Diagnostic {
        fun editSymbol(info: EditInfo, diagnostic: Diagnostic): Diagnostic = when {
            info.replacementText.isEmpty() -> "Invalid text error: replacement text is empty!"
            diagnostic.isNotEmpty() -> "Invalid location error: start and/or end location is invalid!"
            else -> ""
        }

        if (registeredEditList.isEmpty()) return ""
        registeredEditList.sortAndReverse().forEach { info ->
            val errorDiagnostic = editSymbol(info, getDiagnosticOrReplaceText(info))
            if (errorDiagnostic.isNotEmpty()) return errorDiagnostic
        }
        return this.getDiagnosticOrUpdate()
    }

    private fun getLineNumberIndexes(text: String): Map<Int, Int> {
        tailrec fun findNextIndex(result: MutableMap<Int, Int>, count: Int, currentLine: Int, currentIndex: Int) {
            val nextIndex = text.indexOf("\n", currentIndex) + 1
            val nextLine = currentLine + 1

            if (currentLine > count) return
            result[currentLine] = currentIndex
            findNextIndex(result, count, nextLine, nextIndex)
        }

        val result = mutableMapOf<Int, Int>()

        findNextIndex(result, text.count { it == '\n' }, 0, 0)
        return result
    }
}

internal data class EditInfo(val symbol: DocumentSymbol, val replacementText: String)

internal fun MutableList<EditInfo>.sortAndReverse(): List<EditInfo> {
    val comparator: Comparator<EditInfo> =
        compareBy<EditInfo> { it.symbol.range.start.line }.thenBy { it.symbol.range.start.character }
    this.sortWith(comparator)
    return this.reversed()
}
