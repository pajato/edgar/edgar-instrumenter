package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.core.MessageRouter

@Suppress("EXPERIMENTAL_API_USAGE")
internal object ServerLauncher : DiagnosticProducer {
    override var errorDiagnostic: String = ""

    override fun doOperation(spec: ServerSpec): Diagnostic = SourceInstrumenter.run {
        fun launch(router: MessageRouter): Diagnostic {
            fun getStartDiagnostic(): Diagnostic =
                try { router.start(); "" } catch (exc: Exception) { exc.message ?: "No diagnostic available!" }

            doSourceCopy(spec).let { diagnostic -> if (diagnostic.isNotEmpty()) return diagnostic }
            router.logMessage(clientInfoTag, "Starting router with spec: $spec", "ServerLauncher")
            return getStartDiagnostic().ifEmpty { router.getCodeCoverageSupportDiagnostic(spec.id) }
        }

        getRegisteredClient(spec)?.router?.run { launch(this) } ?: getNoClientErrorMessage(spec.id)
    }
}

internal fun doSourceCopy(spec: ServerSpec): Diagnostic = CopyFileDeleter.getDiagnostic(spec).ifEmpty {
    CopyPathCreator.getDiagnostic(spec).ifEmpty { FileCopier.getDiagnostic(spec) }
}
