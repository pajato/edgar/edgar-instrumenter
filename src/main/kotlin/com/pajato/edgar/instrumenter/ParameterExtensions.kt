package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic

internal fun Symbol.getDiagnosticOrRegisterParameterList(editor: Editor): Diagnostic =
    if (detail == parameterListDetail) children.getDiagnosticOrRegisterParameters(editor)
    else getUnsupportedDetailMessage(detail, parameterListDetail)

internal fun Children?.getDiagnosticOrRegisterParameters(editor: Editor): Diagnostic = when {
    this == null -> getNullListMessage()
    isEmpty() -> ""
    any { it.detail != parameterDetail } -> getAllMessage(parameterDetail)
    else -> getDiagnosticOrRegisterAllParameters(editor)
}

internal fun Children?.getDiagnosticOrRegisterAllParameters(editor: Editor): Diagnostic = when {
    this == null -> getNullListMessage()
    else -> map { it.getDiagnosticOrRegisterParameter(editor) }.find { it.isNotEmpty() }.let { it ?: "" }
}

internal fun Symbol.getDiagnosticOrRegisterParameter(editor: Editor): Diagnostic = when {
    children.isNullOrEmpty() -> getInvalidListMessage()
    else -> children!!.map { it.getDiagnosticOrRegisterParameterChild(editor) }.find { it.isNotEmpty() }.let { it ?: "" }
}

internal val parameterChildChoices = setOf(typeReferenceDetail, constantExpressionDetail)

internal fun Symbol.getDiagnosticOrRegisterParameterChild(editor: Editor): Diagnostic = when (detail) {
    typeReferenceDetail -> ""
    constantExpressionDetail -> getDiagnosticOrRegisterWithEdit(editor)
    else -> getUnsupportedDetailMessage(detail, parameterChildChoices)
}

// Error message functions

internal fun getAllMessage(detail: String) = "All the parameters must have detail $detail!"
