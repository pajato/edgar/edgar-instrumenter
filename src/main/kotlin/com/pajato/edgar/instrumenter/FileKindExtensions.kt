package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol

internal const val noTopLevelItemsMessage = "There are no top level constructs in this file!"

private const val packageDirectiveDetail = "KtPackageDirective"
private const val importListDetail = "KtImportList"

internal const val classDetail = "KtClass"
internal const val objectDeclarationDetail = "KtObjectDeclaration"
internal const val propertyDetail = "KtProperty"

// Extension functions for a File kind document symbol.

internal fun Symbol.getDiagnosticOrInstrumentTopLevelSymbols(editor: Editor): Diagnostic = when {
    detail != fileDetail -> getUnsupportedDetailMessage(detail, fileDetail)
    children != null && children!!.isEmpty() -> getInvalidSizeMessage(detail)
    else -> children?.getDiagnosticOrRegisterTopLevelChildren(editor) ?: getNullListMessage()
}

internal fun Children.getDiagnosticOrRegisterTopLevelChildren(editor: Editor): Diagnostic =
    map { it.getDiagnosticOrRegisterTopLevelSymbol(editor) }.find { it.isNotEmpty() }.let { it ?: "" }

internal val topLevelSymbolChoices: DetailSymbolMap = mapOf(
    classDetail to Symbol::getDiagnosticOrRegisterClassKind,
    functionDetail to Symbol::getDiagnosticOrRegisterFunctionKind,
    propertyDetail to Symbol::getDiagnosticOrRegisterPropertyKind,
    objectDeclarationDetail to Symbol::getDiagnosticOrRegisterObjectDeclaration,
)

internal val allTopLevelSymbolChoices = setOf(
    packageDirectiveDetail, importListDetail
).plus(topLevelSymbolChoices.keys)

internal fun Symbol.getDiagnosticOrRegisterTopLevelSymbol(editor: Editor) = when {
    detail == packageDirectiveDetail || detail == importListDetail -> ""
    topLevelSymbolChoices.keys.contains(detail) -> topLevelSymbolChoices[detail]!!.invoke(this, editor)
    else -> getUnsupportedDetailMessage(detail, allTopLevelSymbolChoices)
}

internal fun Symbol.getDiagnosticOrRegisterClassKind(editor: Editor): Diagnostic =
    getDiagnostic(0, 2, classDetail).ifEmpty {
        children.getDiagnosticOrRegister { it.getDiagnosticOrRegisterClassChild(editor) }
    }

internal fun Symbol.getDiagnostic(minSize: Int, maxSize: Int, expectedDetail: String): Diagnostic {
    val kids: List<DocumentSymbol> = children ?: return getNullListMessage()
    return when {
        detail != expectedDetail -> getUnsupportedDetailMessage(detail, expectedDetail)
        kids.size < minSize || kids.size > maxSize -> getInvalidSizeMessage(kids.size, minSize, maxSize)
        else -> ""
    }
}

internal fun Children?.getDiagnosticOrRegister(getDiagnostic: (DocumentSymbol) -> Diagnostic): Diagnostic =
    if (this == null) getNullListMessage() else map { getDiagnostic(it) }.find { it.isNotEmpty() }.let { it ?: "" }

internal fun Symbol.getDiagnosticOrRegisterFunctionKind(editor: Editor): Diagnostic =
    getDiagnostic(2, 3, functionDetail).ifEmpty {
        children.getDiagnosticOrRegister { it.getDiagnosticOrRegisterFunctionChild(editor) }
    }

internal fun Symbol.getDiagnosticOrRegisterPropertyKind(editor: Editor): Diagnostic =
    getDiagnostic(1, 3, propertyDetail).ifEmpty {
        children.getDiagnosticOrRegister { it.getDiagnosticOrRegisterPropertyChild(editor) }
    }

internal fun Symbol.getDiagnosticOrRegisterObjectDeclaration(editor: Editor): Diagnostic =
    getDiagnostic(0, 2, objectDeclarationDetail).ifEmpty {
        children.getDiagnosticOrRegister { it.getDiagnosticOrRegisterObjectDeclarationChild(editor) }
    }

// Error message functions

internal fun getInvalidSizeMessage(size: Int, min: Int, max: Int) =
    "Invalid number of children: $size, expected more than $min and not more than $max!"
