package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import java.net.URI
import java.nio.file.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.toPath

internal object CopyPathCreator : DiagnosticProducer {
    override var errorDiagnostic: String = ""
    internal var throwForTest: Boolean = false

    override fun doOperation(spec: ServerSpec): Diagnostic =
        try { createDirectoriesWithThrow(spec) } catch (exc: Exception) { getExcMessage(exc.message) }

    private fun getExcMessage(message: String?): Diagnostic = "Failed to create directories with message: $message!"

    private fun createDirectoriesWithThrow(spec: ServerSpec): Diagnostic =
        if (throwForTest) throw TestException(testExceptionMessage) else createDirectories(URI(spec.copyUri).toPath())
}

internal class TestException(message: String) : Exception(message)

internal const val testExceptionMessage = "Test exception message."

internal fun createDirectories(path: Path): Diagnostic { path.createDirectories().also { return "" } }
