package com.pajato.edgar.instrumenter

import com.pajato.edgar.collector.getRecordText
import com.pajato.edgar.core.Diagnostic

internal const val constantExpressionDetail = "KtConstantExpression"
internal const val declarationModifierListDetail = "KtDeclarationModifierList"
internal const val stringTemplateExpressionDetail = "KtStringTemplateExpression"
internal const val typeReferenceDetail = "KtTypeReference"

// Extension functions for a Property kind document symbol.

internal val propertyChildChoices: DetailSymbolMap = mapOf(
    stringTemplateExpressionDetail to Symbol::getDiagnosticOrRegisterStringTemplateExpression,
    constantExpressionDetail to Symbol::getDiagnosticOrRegisterWithEdit,
    ifExpressionDetail to Symbol::getDiagnosticOrRegisterIfExpression,
    whenExpressionDetail to Symbol::getDiagnosticOrRegisterWhenExpression,
)

internal val propertyChildListChoices: DetailSymbolListMap = mapOf(
    callExpressionDetail to Symbol::getDiagnosticOrRegisterCallExpression,
)

internal val allPropertyChildChoices: Set<String> = setOf(
    typeReferenceDetail,
    declarationModifierListDetail
).plus(propertyChildChoices.keys).plus(propertyChildListChoices.keys)

internal fun Symbol.getDiagnosticOrRegisterPropertyChild(editor: Editor): Diagnostic = when {
    detail == typeReferenceDetail || detail == declarationModifierListDetail -> ""
    propertyChildChoices.keys.contains(detail) -> propertyChildChoices[detail]!!.invoke(this, editor)
    propertyChildListChoices.keys.contains(detail) ->
        propertyChildListChoices[detail]!!.invoke(this, editor, mutableListOf())
    else -> getUnsupportedDetailMessage(detail, allPropertyChildChoices)
}

internal fun Symbol.getDiagnosticOrRegisterWithEdit(editor: Editor) =
    editor.getDiagnosticOrRegisterSymbol(
        symbol = this,
        replacementText = "${getRecordText()} { ${editor.getText(range.start, range.end)} }"
    )

private fun Symbol.getDiagnosticOrRegisterStringTemplateExpression(editor: Editor): Diagnostic =
    getDiagnosticOrRegisterWithEdit(editor)

internal fun Symbol.getDiagnosticOrRegisterConstantExpression(editor: Editor): Diagnostic =
    getDiagnosticOrRegisterWithEdit(editor)

internal fun Symbol.getDiagnosticOrRegisterConstantExpression(editor: Editor, list: ArgList): Diagnostic =
    if (list.add(" ${getRecordText()} { ${getSelectionText(editor)} }")) "" else getRegisterFailedMessage()

// Error message functions

internal fun Symbol.getRegisterFailedMessage() = "Could not register the symbol with detail: $detail!"
