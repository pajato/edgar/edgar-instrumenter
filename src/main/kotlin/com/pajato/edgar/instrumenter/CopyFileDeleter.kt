package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.nio.file.Path
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.exists
import kotlin.io.path.getLastModifiedTime
import kotlin.io.path.isDirectory
import kotlin.io.path.isRegularFile
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.io.path.toPath

internal const val compressionError = "Failed to compress a previous copy!"
internal const val noCopyError = "The project copy path does not exist!"
internal const val noSourceError = "The project source path does not exist!"
internal const val overwriteError = "Overwrite error: the spec project and copy uri values cannot be identical!"
internal const val partialDeleteError = "The previous project copy failed in the middle of a recursive delete!"

internal object CopyFileDeleter : DiagnosticProducer {
    override var errorDiagnostic: Diagnostic = ""

    override fun doOperation(spec: ServerSpec): Diagnostic {
        val projectPath = URI(spec.projectUri).toPath()
        val copyPath = URI(spec.copyUri).toPath()
        return when {
            !projectPath.exists() -> noSourceError
            !copyPath.exists() -> noCopyError
            !projectPath.isDirectory() -> sourceIsFileError
            !copyPath.isDirectory() -> targetIsFileError
            projectPath == copyPath -> overwriteError
            copyPath.listDirectoryEntries().isEmpty() -> ""
            else -> copyPath.getCopyPrepDiagnostic()
        }
    }
}

internal fun Path.getCopyPrepDiagnostic(): Diagnostic {
    fun compressDirectory(dir: String, zipFileName: String): Diagnostic {
        fun populatePathList(directory: Path, pathList: MutableList<String>) {
            val paths = directory.listDirectoryEntries()
            paths.forEach { path ->
                if (path.isRegularFile()) pathList.add(path.absolutePathString()) else populatePathList(path, pathList)
            }
        }

        fun copyPathListToZipFile(directory: Path, pathList: MutableList<String>): Diagnostic {
            fun copyPathListToZipFile(): Diagnostic {
                fun copyFilePathToZipFile(filePath: String, zos: ZipOutputStream): Diagnostic {
                    fun copyFilePathToZipFile(): Diagnostic {
                        FileInputStream(filePath).use { fis ->
                            val buffer = ByteArray(1024)
                            var length: Int
                            while (fis.read(buffer).also { length = it } > 0) { zos.write(buffer, 0, length) }
                            zos.closeEntry()
                        }
                        return ""
                    }

                    return try { copyFilePathToZipFile() } catch (e: Exception) { "$compressionError: ${e.message}" }
                }

                FileOutputStream(zipFileName).use { fos ->
                    ZipOutputStream(fos).use { zos ->
                        for (filePath in pathList) {
                            val name = filePath.substring(directory.absolutePathString().length + 1, filePath.length)
                            val zipEntry = ZipEntry(name)
                            zos.putNextEntry(zipEntry)
                            copyFilePathToZipFile(filePath, zos)
                        }
                    }
                }
                return ""
            }

            fun moveToBuildDir(): Diagnostic {
                val currentDir = File(System.getProperty("user.dir"))
                val zipFile = File(currentDir, zipFileName)
                val destFile = File(currentDir, "/build/$zipFileName")
                return if (zipFile.renameTo(destFile)) "" else "The zip file rename failed!"
            }

            val result = try { copyPathListToZipFile() } catch (e: IOException) { "$compressionError: ${e.message}" }
            return result.ifEmpty { moveToBuildDir() }
        }

        val directory = Path(dir)
        val pathList: MutableList<String> = ArrayList()

        populatePathList(directory, pathList)
        return copyPathListToZipFile(directory, pathList)
    }

    fun removeCopy(): Diagnostic = if (toFile().deleteRecursively()) "" else partialDeleteError

    val name = "$name-${getLastModifiedTime()}.zip"
    return compressDirectory(absolutePathString(), name).ifEmpty { removeCopy() }
}
