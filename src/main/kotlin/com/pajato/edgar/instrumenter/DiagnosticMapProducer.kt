package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec

/**
 * Indicates that an implementing object produces a map of diagnostic messages. Used to handle multiple errors.
 */
interface DiagnosticMapProducer {
    /**
     * The map associating some condition descriptor with a diagnostic message.
     */
    val diagnosticMap: DiagnosticMap

    /**
     * The operation that may produce multiple error messages.
     */
    fun doOperation(spec: ServerSpec): DiagnosticMap

    /**
     * The preferred way to collect and report on error diagnostics.
     */
    fun hasDiagnostic(spec: ServerSpec): Boolean {
        diagnosticMap.clear()
        diagnosticMap.putAll(doOperation(spec))
        return diagnosticMap.isNotEmpty()
    }
}
