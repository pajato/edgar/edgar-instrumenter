package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.DocumentSymbolParams
import com.pajato.isaac.api.DocumentSymbolResult
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.api.TextDocumentIdentifier
import com.pajato.isaac.core.DocumentSymbolExecutor
import com.pajato.isaac.core.MessageRouter
import java.io.File

internal const val fileDetail = "KtFile"

internal val editorList: MutableList<Editor> = mutableListOf()

internal fun getNoClientErrorMessage(id: String) = "There is no client available using id: $id!"

internal object FileInstrumenter : DiagnosticProducer {
    lateinit var relativePath: String
    override var errorDiagnostic: String = ""

    override fun doOperation(spec: ServerSpec): Diagnostic {
        val client = SourceInstrumenter.registeredClientMap[spec.id] ?: return getNoClientErrorMessage(spec.id)
        val router = client.router

        fun doDocumentSymbols(router: MessageRouter): Diagnostic {
            val targetFile = File(spec.copyUri.substring(5) + relativePath)
            val uri: DocumentUri = "file://${targetFile.absolutePath}"
            val id = TextDocumentIdentifier(uri)
            val documentSymbolParams = DocumentSymbolParams(textDocument = id)
            val textChanger = FileEditor(targetFile)

            fun updateError(response: ResponseMessage, editor: Editor) {
                errorDiagnostic = response.getInstrumentingDiagnostic(router, editor)
            }

            editorList.add(textChanger)
            errorDiagnostic = "No response returned from language server!"
            DocumentSymbolExecutor(client).execute(documentSymbolParams) { updateError(it, textChanger) }
            return errorDiagnostic.ifEmpty { textChanger.getDiagnosticOrPerformEdits() }
        }

        return if (this::relativePath.isInitialized) doDocumentSymbols(router) else "No file path available! Aborting."
    }
}

internal fun ResponseMessage.getInstrumentingDiagnostic(router: MessageRouter, editor: Editor): Diagnostic {
    fun getDiagnosticOrHandleTopLevelSymbols(symbol: DocumentSymbol, indentation: Int = 0): Diagnostic {
        if (symbol.detail != fileDetail) return "Invalid first element: ${symbol.detail}, expected KtFile"
        return symbol.run {
            log(editor, router, " ".repeat(indentation))
            getDiagnosticOrInstrumentTopLevelSymbols(editor)
        }
    }

    val result = result // Snapshot the result to make it a local variable.
    return when {
        result == null -> "Unable to obtain a valid result for file: ${editor.file}"
        result !is DocumentSymbolResult -> "Unexpected response result type: ${result.javaClass.simpleName}"
        result.list.isEmpty() -> "Expected a single top level document symbol; found none."
        else -> getDiagnosticOrHandleTopLevelSymbols(result.list[0])
    }
}
