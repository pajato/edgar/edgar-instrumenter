package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import java.io.File

/**
 * Provide a means to access for possible change, a snippet from the source text being instrumented.
 */
interface Editor {
    /**
     * Get the substring given the start and end positions. Return the empty string for invalid positions.
     */
    fun getText(start: Position, end: Position): String

    /**
     * Replace the text bounded by start and end with the replacement text.
     */
    fun getDiagnosticOrReplaceText(replacement: String, start: Position, end: Position): Diagnostic

    /**
     * Convert a Position to a string index.
     */
    fun getIndex(position: Position): Int

    /**
     * Update the text back to its origin reporting any errors.
     */
    fun getDiagnosticOrUpdate(): Diagnostic

    /**
     * Convert an LSP Position to a file/string index.
     */
    fun convert(position: Position): Int

    /**
     * Associate an executable LSP document symbol with a counter to record the number of times that symbol was
     * executed.
     */
    fun getDiagnosticOrRegisterInitialCountValue(symbol: DocumentSymbol): Diagnostic

    /**
     * Register an LSP document symbol and its replacement text to be recorded by the data collector.
     */
    fun getDiagnosticOrRegisterSymbol(symbol: DocumentSymbol, replacementText: String): Diagnostic

    /**
     * Perform the registered edits (instrumentation) to the file.
     */
    fun getDiagnosticOrPerformEdits(): Diagnostic

    /**
     * The file being instrumented
     */
    val file: File

    /**
     * The source file text being instrumented.
     */
    var text: String
}
