package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic

internal fun Symbol.getDiagnosticOrRegisterObjectDeclarationChild(editor: Editor): Diagnostic =
    when (detail) {
        classBodyDetail -> children.getDiagnosticOrRegisterClassBody(editor)
        else -> getUnsupportedDetailMessage(detail, classBodyDetail)
    }
