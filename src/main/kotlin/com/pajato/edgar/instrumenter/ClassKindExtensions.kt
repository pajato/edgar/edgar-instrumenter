package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol

internal typealias Symbol = DocumentSymbol
internal typealias Children = List<DocumentSymbol>
internal typealias DetailChildrenMap = Map<String, Children?.(Editor) -> Diagnostic>

internal const val classBodyDetail = "KtClassBody"
internal const val classInitializerDetail = "KtClassInitializer"
internal const val primaryConstructorDetail = "KtPrimaryConstructor"
internal const val superTypeEntryDetail = "KtSuperTypeEntry"
internal const val superTypeListDetail = "KtSuperTypeList"

internal val classChildChoiceMap: DetailChildrenMap = mapOf(
    primaryConstructorDetail to Children?::getDiagnosticOrRegisterConstructor,
    classBodyDetail to Children?::getDiagnosticOrRegisterClassBody,
)

internal fun Symbol.getDiagnosticOrRegisterClassChild(editor: Editor): Diagnostic = when {
    classChildChoiceMap.keys.contains(detail) -> classChildChoiceMap[detail]!!.invoke(children, editor)
    detail == superTypeListDetail -> this.children.getDiagnosticOrRegisterSuperTypeList()
    else -> getUnsupportedDetailMessage(detail, classChildChoiceMap.keys.plus(superTypeListDetail))
}

internal fun Children?.getDiagnosticOrRegisterConstructor(editor: Editor): Diagnostic = when {
    isNullOrEmpty() -> getInvalidListMessage()
    size > 1 -> getInvalidSizeMessage(size, 1)
    else -> this[0].getDiagnosticOrRegisterParameterList(editor)
}

internal fun Children.getDiagnosticOrRegisterElements(editor: Editor): Diagnostic =
    this.map { it.getDiagnosticOrRegisterElement(editor) }.find { it.isNotEmpty() }.let { it ?: "" }

internal val elementChoiceMap: Map<String, Symbol.(Editor) -> Diagnostic> = mapOf(
    classInitializerDetail to Symbol::getDiagnosticOrRegisterClassInitializer,
    functionDetail to Symbol::getDiagnosticOrRegisterFunctionKind,
    propertyDetail to Symbol::getDiagnosticOrRegisterPropertyKind,
)

internal fun Symbol.getDiagnosticOrRegisterElement(editor: Editor): Diagnostic = when {
    elementChoiceMap.keys.contains(detail) -> elementChoiceMap[detail]!!.invoke(this, editor)
    else -> getUnsupportedDetailMessage(detail, elementChoiceMap.keys)
}

internal fun Children?.getDiagnosticOrRegisterClassBody(editor: Editor): Diagnostic = when {
    this == null -> getNullListMessage()
    isEmpty() -> ""
    else -> getDiagnosticOrRegisterElements(editor)
}

internal fun Symbol.getDiagnosticOrRegisterClassInitializer(editor: Editor): Diagnostic = when {
    detail != classInitializerDetail -> getUnsupportedDetailMessage(detail, classInitializerDetail)
    children == null -> getNullListMessage()
    children!!.size != 1 -> getInvalidSizeMessage(children!!.size, 1)
    children!![0].detail != blockExpressionDetail ->
        getUnsupportedDetailMessage(children!![0].detail, blockExpressionDetail)
    else -> children!![0].getDiagnosticOrRegisterBlockExpression(editor)
}

internal fun Children?.getDiagnosticOrRegisterSuperTypeList(): Diagnostic = when {
    isNullOrEmpty() -> getInvalidListMessage()
    else -> map { it.getDiagnosticOrRegisterSuperTypeEntries() }.find { it.isNotEmpty() }.let { it ?: "" }
}

internal fun Symbol.getDiagnosticOrRegisterSuperTypeEntries(): Diagnostic = when (detail) {
    superTypeEntryDetail -> ""
    else -> getUnsupportedDetailMessage(detail, superTypeEntryDetail)
}

// Error message functions

internal fun getInvalidListMessage() = "Invalid list: null or empty!"

internal fun getUnsupportedDetailMessage(detail: String?, expected: String) =
    "Unsupported element with detail ($detail), expected ($expected)!"

internal fun getUnsupportedDetailMessage(detail: String?, expected: Set<String>) =
    "Unsupported element with detail ($detail), expected one of (${expected.joinToString()})!"

internal fun getNullListMessage() = "Invalid list: null!"

internal fun getInvalidSizeMessage(detail: String?) =
    "Expected at least one child for the symbol with detail $detail!"

internal fun getInvalidSizeMessage(size: Int, expected: Int) =
    "Invalid number of children: $size, expected $expected!"

/* internal fun getInternalSoftwareErrorMessage(detail: String = "") =
    "Internal software error: ${detail.ifEmpty { "no reason given" }}!" */
