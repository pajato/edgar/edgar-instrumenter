package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic

internal const val callExpressionDetail = "KtCallExpression"
internal const val nameReferenceExpressionDetail = "KtNameReferenceExpression"
internal const val valueArgumentListDetail = "KtValueArgumentList"

internal fun Symbol.getDiagnosticOrRegisterCallExpression(editor: Editor, list: ArgList): Diagnostic = when {
    children == null -> getNullListMessage()
    children!!.size != 2 -> getInvalidSizeMessage(children!!.size, 2)
    children!![0].detail != nameReferenceExpressionDetail -> getInvalidCallExpressionNameMessage()
    children!![1].detail != valueArgumentListDetail -> getInvalidCallExpressionArgListMessage()
    else -> children!!.getDiagnosticOrRegisterCallExpressionValueArgumentList(editor, list).ifEmpty {
        val newText = list.joinToString(prefix = "${children!![0].getRangeText(editor)}(", postfix = ")")
        getDiagnosticOrRegisterWithText(editor, newText)
    }
}

internal fun Children.getDiagnosticOrRegisterCallExpressionValueArgumentList(
    editor: Editor,
    list: ArgList
): Diagnostic = if (size != 2) getInvalidSizeMessage(size, 2) else
    this[1].getDiagnosticOrRegisterValueArgumentList(editor, list)

internal fun Symbol.getDiagnosticOrRegisterValueArgumentList(editor: Editor, list: ArgList): Diagnostic =
    if (children == null) getNullListMessage() else children!!.map {
        it.getDiagnosticOrRegisterValueArgument(editor, list)
    }.find { it.isNotEmpty() }.let { it ?: "" }

// Error message functions

internal fun getInvalidCallExpressionArgListMessage() =
    "Invalid call expression document symbol: missing argument list!"

internal fun getInvalidCallExpressionNameMessage() = "Invalid call expression document symbol: missing name!"
