package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import java.io.File
import java.net.URI
import kotlin.io.path.createTempDirectory
import kotlin.io.path.toPath

internal const val invalidCopyUriErrorMessage = "The copy URI is not a file!"
internal const val invalidProjectUriErrorMessage = "The project URI is not a file!"
internal const val renameError = "Could not rename the temp file to the destination!"
internal const val sourceIsFileError = "The source file must be a directory!"
internal const val targetIsFileError = "The target file must be a directory!"

internal fun getFileOrNull(uri: String): File? = try { URI(uri).toPath().toFile() } catch (exc: Exception) { null }

internal object FileCopier : DiagnosticProducer {
    override var errorDiagnostic: String = ""

    override fun doOperation(spec: ServerSpec): Diagnostic {
        val source: File = getFileOrNull(spec.projectUri) ?: return invalidProjectUriErrorMessage
        val target: File = getFileOrNull(spec.copyUri) ?: return invalidCopyUriErrorMessage

        fun validate(): Diagnostic = when {
            !source.isDirectory -> sourceIsFileError
            target.exists() && !target.isDirectory -> targetIsFileError
            source.absolutePath == target.absolutePath -> overwriteError
            else -> ""
        }

        fun renameCopy(tempDir: File): Diagnostic {
            source.doCopy(tempDir)
            return if (tempDir.renameTo(target)) "" else renameError
        }

        errorDiagnostic = validate().ifEmpty { renameCopy(createTempDirectory().toFile()) }
        return errorDiagnostic
    }
}

internal fun File.doCopy(tempDir: File): Diagnostic {
    var errorDiagnostic: Diagnostic = ""

    fun captureError(file: File, exc: Exception): OnErrorAction {
        errorDiagnostic = "Copy failed on file: ${file.absolutePath}, with message: ${exc.message}"
        return OnErrorAction.TERMINATE
    }

    return if (copyRecursively(tempDir, false) { file, exc -> captureError(file, exc) }) "" else errorDiagnostic
}
