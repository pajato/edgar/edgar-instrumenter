package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol

internal const val whenConditionWithExpression = "KtWhenConditionWithExpression"
internal const val whenEntryDetail = "KtWhenEntry"
internal const val whenExpressionDetail = "KtWhenExpression"

internal fun DocumentSymbol.getDiagnosticOrRegisterWhenExpression(editor: Editor): Diagnostic = when {
    detail != whenExpressionDetail -> getUnsupportedDetailMessage(detail, whenExpressionDetail)
    children == null -> getNullListMessage()
    children!!.isEmpty() -> getInvalidSizeMessage(detail)
    else -> children!!.map { it.getDiagnosticOrRegisterWhenEntries(editor) }.find { it.isNotEmpty() }
        .let { it ?: "" }
}

internal fun DocumentSymbol.getDiagnosticOrRegisterWhenEntries(editor: Editor): Diagnostic = when {
    detail != whenEntryDetail -> getUnsupportedDetailMessage(detail, whenEntryDetail)
    children == null -> getNullListMessage()
    children!!.isEmpty() -> getInvalidSizeMessage(detail)
    else -> children!!.map { it.getDiagnosticOrRegisterWhenEntryChild(editor) }.find { it.isNotEmpty() }
        .let { it ?: "" }
}

internal fun DocumentSymbol.getDiagnosticOrRegisterWhenEntryChild(editor: Editor): Diagnostic = when {
    children == null -> getNullListMessage()
    children!!.size != 1 -> getInvalidSizeMessage(children!!.size, 1)
    detail == whenConditionWithExpression -> children!![0].registerWhenConditionWithExpression(editor)
    detail == stringTemplateExpressionDetail -> getDiagnosticOrRegisterWithEdit(editor)
    else -> getUnsupportedDetailMessage(detail, "$whenConditionWithExpression or $stringTemplateExpressionDetail")
}

internal fun DocumentSymbol.registerWhenConditionWithExpression(editor: Editor): Diagnostic = when (detail) {
    constantExpressionDetail -> getDiagnosticOrRegisterWithEdit(editor)
    else -> getUnsupportedDetailMessage(detail, constantExpressionDetail)
}
