package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ConfSpec
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.client.Client
import java.net.URI
import kotlin.io.path.toPath

typealias DiagnosticMap = MutableMap<String, Diagnostic>
typealias ServerFunc = (List<ServerSpec>) -> DiagnosticMap

object SourceInstrumenter {
    val registeredClientMap: MutableMap<String, Client> = mutableMapOf()

    fun getRegisteredClient(spec: ServerSpec): Client? = registeredClientMap[spec.id]

    fun doInstrumentation(specs: List<ConfSpec>): DiagnosticMap {
        val serverSpecList = specs.filterIsInstance<ServerSpec>()
        val funcList = listOf<ServerFunc>(::startLanguageServers, ::instrumentFiles, ::stopLanguageServers)

        check(serverSpecList.isNotEmpty()) { "No servers configured! Aborting!" }
        funcList.forEach { func -> func.invoke(serverSpecList).let { if (it.isNotEmpty()) return it } }
        return mutableMapOf()
    }

    private tailrec fun startLanguageServers(specList: List<ServerSpec>): DiagnosticMap = when {
        specList.isEmpty() -> mutableMapOf()
        ServerLauncher.hasDiagnostic(specList[0]) -> specList[0].copyUri.toMap(ServerLauncher.errorDiagnostic)
        else -> startLanguageServers(specList.subList(1, specList.size))
    }

    private tailrec fun instrumentFiles(specList: List<ServerSpec>): DiagnosticMap = when {
        specList.isEmpty() -> mutableMapOf()
        ProjectFilesInstrumenter.hasDiagnostic(specList[0]) -> ProjectFilesInstrumenter.diagnosticMap
        else -> instrumentFiles(specList.subList(1, specList.size))
    }

    private fun stopLanguageServers(specList: List<ServerSpec>): DiagnosticMap = with(registeredClientMap) {
        specList.mapNotNull { if (containsKey(it.id)) this[it.id] else null }.forEach { it.router.stop() }.let {
            mutableMapOf()
        }
    }
}

fun String.toMap(error: Diagnostic): DiagnosticMap {
    val file = URI(this).toPath().toFile()
    return mutableMapOf(file.absolutePath to error)
}
