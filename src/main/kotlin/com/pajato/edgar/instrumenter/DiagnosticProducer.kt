package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec

@Suppress("BooleanMethodIsAlwaysInverted")
interface DiagnosticProducer {
    /**
     * An empty string indicating no errors have occurred or a non-empty string explaining what went wrong.
     */
    var errorDiagnostic: Diagnostic

    /**
     * Perform an operation that might produce an error message (diagnostic).
     */
    fun doOperation(spec: ServerSpec): Diagnostic

    /**
     * A convenience operation to both perform an operation and indicate if it succeeded (true) or failed (false).
     */
    fun hasDiagnostic(spec: ServerSpec): Boolean {
        errorDiagnostic = doOperation(spec)
        return errorDiagnostic.isNotEmpty()
    }

    /**
     * An alternative (and experimental) approach to getting the diagnostic from the producer.
     */
    fun getDiagnostic(spec: ServerSpec): Diagnostic = doOperation(spec)
}
