package com.pajato.edgar.instrumenter

import com.pajato.edgar.collector.getRecordText
import com.pajato.edgar.core.Diagnostic

internal typealias DetailSymbolMap = Map<String, Symbol.(Editor) -> Diagnostic>
internal typealias DetailSymbolListMap = Map<String, Symbol.(Editor, MutableList<String>) -> Diagnostic>

internal const val blockExpressionDetail = "KtBlockExpression"
internal const val dotQualifiedExpressionDetail = "KtDotQualifiedExpression"
internal const val functionDetail = "KtNamedFunction"
internal const val ifExpressionDetail = "KtIfExpression"
internal const val parameterListDetail = "KtParameterList"
internal const val parameterDetail = "KtParameter"
internal const val postfixExpressionDetail = "KtPostfixExpression"
internal const val valueArgumentDetail = "KtValueArgument"

// Extension functions for a (named) Function kind document symbol.

internal val functionChildMap: DetailSymbolMap = mapOf(
    parameterListDetail to Symbol::getDiagnosticOrRegisterParameterList,
    blockExpressionDetail to Symbol::getDiagnosticOrRegisterBlockExpression,
)

internal fun Symbol.getDiagnosticOrRegisterFunctionChild(editor: Editor): Diagnostic = when {
    detail == declarationModifierListDetail -> ""
    functionChildMap.keys.contains(detail) -> functionChildMap[detail]!!.invoke(this, editor)
    else -> getUnsupportedDetailMessage(detail, setOf(declarationModifierListDetail).plus(functionChildMap.keys))
}

internal fun Symbol.getDiagnosticOrRegisterBlockExpression(editor: Editor): Diagnostic = when (children) {
    null -> getNullListMessage()
    else -> children!!.map { it.getDiagnosticOrRegisterStatementEdits(editor) }.find { it.isNotEmpty() }
        .let { it ?: "" }
}

internal val statementListChoices: DetailSymbolListMap = mapOf(
    callExpressionDetail to Symbol::getDiagnosticOrRegisterCallExpression,
    binaryExpressionDetail to Symbol::getDiagnosticOrRegisterBinaryExpression,
)

internal val statementNoListChoices: DetailSymbolMap = mapOf(
    postfixExpressionDetail to Symbol::getDiagnosticOrRegisterConstantExpression,
    dotQualifiedExpressionDetail to Symbol::getDiagnosticOrRegisterConstantExpression,
)

internal fun Symbol.getDiagnosticOrRegisterStatementEdits(editor: Editor): Diagnostic = when {
    detail == null -> getInvalidDetailMessage(null, "non-null")
    statementListChoices.keys.contains(detail) ->
        statementListChoices[detail]!!.invoke(this, editor, mutableListOf())
    statementNoListChoices.keys.contains(detail) -> statementNoListChoices[detail]!!.invoke(this, editor)
    else -> getUnsupportedDetailMessage(detail, statementListChoices.keys.plus(statementNoListChoices.keys))
}

internal fun Symbol.getDiagnosticOrRegisterWithText(editor: Editor, text: String) =
    editor.getDiagnosticOrRegisterSymbol(this, "${getRecordText()} { $text }")

internal val valueArgumentChoiceMap: Map<String, Symbol.(Editor, MutableList<String>) -> Diagnostic> = mapOf(
    stringTemplateExpressionDetail to Symbol::getDiagnosticOrRegisterWithList,
    nameReferenceExpressionDetail to Symbol::getDiagnosticOrRegisterWithList,
    dotQualifiedExpressionDetail to Symbol::getDiagnosticOrRegisterWithList,
)

internal fun Symbol.getDiagnosticOrRegisterValueArgument(
    editor: Editor,
    list: MutableList<String>,
): Diagnostic = when {
    detail != valueArgumentDetail -> getInvalidDetailMessage(detail, valueArgumentDetail)
    children.isNullOrEmpty() -> getInvalidSizeMessage(0, 1)
    valueArgumentChoiceMap.keys.contains(children!![0].detail) ->
        valueArgumentChoiceMap[children!![0].detail]!!.invoke(this, editor, list)
    else -> getUnsupportedDetailMessage(children!![0].detail, valueArgumentChoiceMap.keys)
}

internal fun Symbol.getDiagnosticOrRegisterWithList(editor: Editor, list: MutableList<String>): Diagnostic {
    list.add("${getRecordText()} { ${editor.getText(range.start, range.end)} }")
    return editor.getDiagnosticOrRegisterInitialCountValue(this)
}

internal fun Symbol.getDiagnosticOrRegisterIfExpression(editor: Editor): Diagnostic = when {
    detail != ifExpressionDetail -> getUnsupportedDetailMessage(detail, ifExpressionDetail)
    children == null -> getNullListMessage()
    children!!.size != 3 -> getInvalidSizeMessage(children!!.size, 3)
    else -> children!!.getDiagnosticOrRegisterIfExpressionChildren(editor)
}

internal fun Children.getDiagnosticOrRegisterIfExpressionChildren(editor: Editor): Diagnostic {
    this[0].getDiagnosticOrRegisterWithEdit(editor)
    this[1].getDiagnosticOrRegisterWithEdit(editor)
    this[2].getDiagnosticOrRegisterWithEdit(editor)
    return ""
}

// Error message functions

internal fun getInvalidDetailMessage(detail: String?, expected: String) =
    "Invalid document symbol with detail: $detail; expected $expected!"
