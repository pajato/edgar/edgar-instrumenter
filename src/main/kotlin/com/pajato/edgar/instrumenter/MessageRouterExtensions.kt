package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.BooleanDocumentSymbolProvider
import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.OptionsDocumentSymbolProvider
import com.pajato.isaac.core.MessageRouter

internal const val noInitializeResult = "no initialize result was returned!"
internal const val noDocumentSymbolSupport = "it lacks document symbol support"

internal fun MessageRouter.getCodeCoverageSupportDiagnostic(id: String): Diagnostic {
    val result = initializeResult

    fun supportsCodeCoverage(result: InitializeResult): Boolean {
        val provider = result.capabilities.documentSymbolProvider ?: return false
        return when (provider) {
            is BooleanDocumentSymbolProvider -> provider.state
            is OptionsDocumentSymbolProvider -> false // This is broken in the API!
        }
    }

    return when {
        result == null -> getCodeCoverageMessage(id, noInitializeResult)
        !supportsCodeCoverage(result) -> getCodeCoverageMessage(id, noDocumentSymbolSupport)
        else -> ""
    }
}

internal fun getCodeCoverageMessage(id: String, suffix: String) =
    "Language server $id does not support code coverage because $suffix"
