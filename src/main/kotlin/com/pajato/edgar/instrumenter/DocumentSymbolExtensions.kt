package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.core.MessageRouter

internal fun DocumentSymbol.getRangeText(editor: Editor) = editor.getText(range.start, range.end)

internal fun DocumentSymbol.getSelectionText(editor: Editor) =
    editor.getText(selectionRange.start, selectionRange.end)

internal fun DocumentSymbol.log(editor: Editor, router: MessageRouter, prefix: String) {
    val subsystem = detail ?: return
    val nextPrefix = "    $prefix"

    fun showDefaultSymbol(): String {
        val range = "${prefix}range: " + getRangeText(editor)
        val selection = "${prefix}selection: " + getSelectionText(editor)
        return "${prefix}detail: $detail\n$range\n$selection"
    }

    router.logMessage(clientInfoTag, showDefaultSymbol(), subsystem)
    children?.forEach { it.log(editor, router, nextPrefix) }
}
