package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import java.io.File
import java.net.URI
import kotlin.io.path.toPath

internal object ProjectFilesInstrumenter : DiagnosticMapProducer {
    override val diagnosticMap: DiagnosticMap = mutableMapOf()

    override fun doOperation(spec: ServerSpec): DiagnosticMap {
        val targetDir = URI(spec.copyUri).toPath().toFile()

        fun isCode(file: File) = file.isFile && file.absolutePath.endsWith(".${spec.extension}") &&
            !file.absolutePath.contains("/test/")

        fun instrumentFile(file: File): Diagnostic {
            FileInstrumenter.relativePath = file.absolutePath.substring(targetDir.absolutePath.length + 1)
            return if (FileInstrumenter.hasDiagnostic(spec)) FileInstrumenter.errorDiagnostic else ""
        }

        fun instrument(file: File) =
            instrumentFile(file).let { if (it.isNotEmpty()) diagnosticMap[file.absolutePath] = it }

        diagnosticMap.clear()
        targetDir.walk().filter { isCode(it) }.forEach { file -> instrument(file) }
        return diagnosticMap
    }
}
