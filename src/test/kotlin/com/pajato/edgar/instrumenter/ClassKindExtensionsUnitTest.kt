package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind
import com.pajato.isaac.api.SymbolKind.Class
import java.io.File
import kotlin.io.path.createTempFile
import kotlin.test.Test
import kotlin.test.assertEquals

class ClassKindExtensionsUnitTest : InstrumenterTestProfiler() {

    private val editor: Editor = FileEditor(file = createTempFile().toFile().apply { deleteOnExit() })

    @Test fun `Force an error on the basic diagnostic check`() {
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = null, range = range)
        val expected = getNullListMessage()
        val actual = symbol.getDiagnosticOrRegisterClassKind(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail error by using an invalid child detail`() {
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val range = Range(Position(), Position())
        val child = DocumentSymbol("child", Class, detail = "noSuchDetail", range = range)
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(child), range = range)
        val expected = getUnsupportedDetailMessage(child.detail, classChildChoiceMap.keys.plus(superTypeListDetail))
        val actual = symbol.getDiagnosticOrRegisterClassKind(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid list error by using a null children property`() {
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = null, range = range)
        val expected = getInvalidListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterConstructor(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid list error by using a children property with too many elements`() {
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val range = Range(Position(), Position())
        val child = DocumentSymbol("child", Class, detail = primaryConstructorDetail, range = range)
        val children = listOf(child, child)
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, range = range, children = children)
        val expected = getInvalidSizeMessage(2, 1)
        val actual = symbol.children.getDiagnosticOrRegisterConstructor(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a when case for code coverage`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(), range = range)
        val expected = getInvalidListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterSuperTypeList()
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid list error by using an empty children property`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(), range = range)
        val expected = getInvalidListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterSuperTypeList()
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail error`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = "noSuchDetail", children = listOf(), range = range)
        val expected = getUnsupportedDetailMessage(noSuchDetail, superTypeEntryDetail)
        val actual = symbol.getDiagnosticOrRegisterSuperTypeEntries()
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail error when registering elements`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("symbol", SymbolKind.File, detail = noSuchDetail, range = range)
        val expected = getUnsupportedDetailMessage(symbol.detail, elementChoiceMap.keys)
        val actual = symbol.getDiagnosticOrRegisterElement(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null list error when registering a class body`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("symbol", SymbolKind.File, detail = fileDetail, range = range)
        val expected = getNullListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterClassBody(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail error when registering a class initializer`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("symbol", SymbolKind.File, detail = noSuchDetail, range = range)
        val expected = getUnsupportedDetailMessage(symbol.detail, classInitializerDetail)
        val actual = symbol.getDiagnosticOrRegisterClassInitializer(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null list error when registering a class initializer with null children`() {
        val name = "symbol"
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol(name, kind = SymbolKind.File, detail = classInitializerDetail, range = range)
        val expected = getNullListMessage()
        val actual = symbol.getDiagnosticOrRegisterClassInitializer(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid size error when registering a class initializer with no children`() {
        val name = "symbol"
        val range = Range(Position(), Position())
        val detail = classInitializerDetail
        val symbol = DocumentSymbol(name, kind = SymbolKind.File, detail = detail, range = range, children = listOf())
        val expected = getInvalidSizeMessage(0, 1)
        val actual = symbol.getDiagnosticOrRegisterClassInitializer(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported error when registering a class initializer with an invalid child`() {
        val name = "symbol"
        val range = Range(Position(), Position())
        val child = DocumentSymbol("xyzzy", SymbolKind.Function, detail = "noSuchDetail", range = range)
        val kids = listOf(child)
        val detail = classInitializerDetail
        val symbol = DocumentSymbol(name, SymbolKind.File, detail = detail, range = range, children = kids)
        val expected = getUnsupportedDetailMessage(child.detail, blockExpressionDetail)
        val actual = symbol.getDiagnosticOrRegisterClassInitializer(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }
}
