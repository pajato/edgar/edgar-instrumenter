package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.MessageRouter
import kotlin.test.Test
import kotlin.test.assertEquals

class ServerLauncherUnitTest : InstrumenterTestProfiler() {

    @Test fun `When using a non-registered client, verify correct behavior`() {
        val spec = ServerSpec(fwcdKLS, "kt", "Instrumenter", "/xyzzy", "/xyzzy")
        assertEquals("There is no client available using id: $fwcdKLS!", ServerLauncher.doOperation(spec))
    }

    @Test fun `When a bogus id is used, verify correct diagnostic`() {
        val bogusId = "bogus us"
        val router: MessageRouter = TestMessageRouter()
        val expectedDiagnostic = getCodeCoverageMessage(bogusId, noDocumentSymbolSupport)
        val actualDiagnostic = router.getCodeCoverageSupportDiagnostic(bogusId)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(" coverage support diagnostic"))
    }
}
