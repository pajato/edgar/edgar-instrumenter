package com.pajato.edgar.instrumenter

import com.pajato.edgar.collector.DataCollector
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.core.CoreTestProfiler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalCoroutinesApi
class InstrumenterIntegrationTest : CoreTestProfiler() {
    private val expected: DiagnosticMap = mutableMapOf()

    @BeforeTest fun setup() {
        SourceInstrumenter.registeredClientMap.clear()
        DataCollector.clear()
    }

    @Ignore
    @Test fun `When exploring, verify the results`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val (id, projectDir, projectName) = Triple(fwcdKLS, "kotlin_gradle", "explore")
        val spec: ServerSpec = classLoader.getServerSpec(id, projectDir, projectName)
        val packagePath = "samples"
        activateClient(spec, testRegistryPath)
        doAsserts(spec, packagePath, fileNames = arrayOf("explore"))
    }

    @Ignore
    @Test fun `When kotlin top level properties are exercised, verify the results`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val (id, projectDir, projectName) = Triple(fwcdKLS, "kotlin_gradle", "basics")
        val spec: ServerSpec = classLoader.getServerSpec(id, projectDir, projectName)
        val packagePath = "samples"
        val fileNames = arrayOf("varWithNoInitializer", "varWithStringInitializer", "varWithFunctionInitializer")
        expected[getPath(spec)] = noTopLevelItemsMessage
        activateClient(spec, testRegistryPath)
        doAsserts(spec, packagePath, *fileNames)
    }

    @Ignore
    @Test fun `When the Kotlin basic constructs are instrumented verify the results`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        activateClient(spec, testRegistryPath)
        doAsserts(spec, "samples", "basics", "hw")
    }

    @Ignore
    @Test fun `When instrumenting the instrumenter, verify the result`() {
        val projectCopyDir = "${defaultBuildCopyDir}instrumenter/"
        val copyUri: DocumentUri = "file:${System.getProperty("user.dir")}$projectCopyDir"
        val projectUri: DocumentUri = "file:${System.getProperty("user.dir")}"
        val spec = ServerSpec(fwcdKLS, "kt", "instrumenter", projectUri, copyUri)
        activateClient(spec, testRegistryPath)
        doAsserts(spec, packagePath = "com/pajato/edgar/instrumenter", fileNames = arrayOf("SourceInstrumenter"))
    }

    private fun getPath(spec: ServerSpec): String {
        val dir = "${spec.copyUri}src/main/kotlin/samples".substring(5)
        return File(dir, "varWithNoInitializer.${spec.extension}").absolutePath
    }

    private fun doAsserts(spec: ServerSpec, packagePath: String, vararg fileNames: String) {
        fun assertInstrumentedSource(fileName: String) {
            fun getSource(dir: String, suffix: String): String = File(dir.substring(5), "$fileName$suffix").readText()

            val actualSource = getSource("${spec.copyUri}src/main/kotlin/$packagePath", ".kt")
            val expectedSource = getSource("${this::class.java.classLoader.getResource("instrumenter")!!}", ".txt")
            assertEquals(expectedSource, actualSource, "The instrumented file: ($fileName) content is wrong!")
        }

        fun assertDiagnostics(actual: DiagnosticMap) {
            assertEquals(expected.size, actual.size, "The computed diagnostic map size is wrong!")
            expected.keys.forEach { assertEquals(expected[it], actual[it], "The map content is wrong for key: $it!") }
        }

        val actual: DiagnosticMap = SourceInstrumenter.doInstrumentation(listOf(spec))
        assertDiagnostics(actual)
        fileNames.forEach { fileName -> assertInstrumentedSource(fileName) }
    }
}
