package com.pajato.edgar.instrumenter

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.test.BeforeTest
import kotlin.test.Test

@ExperimentalCoroutinesApi
class BasicPropertyKindUnitTest : InstrumenterTestProfiler() {

    @BeforeTest fun setup() { activateClientWithCache(testServerSpec, testRegistryPath) }

    @Test fun `When executing a variable initialization with a constant, verify a correct result`() {
        val expression = "val foo = 12"
        val snippetCount = 1
        val expected = """val foo = com.pajato.edgar.collector.DataCollector.record(0) { 12 }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a constant initialization, verify a correct result`() {
        val expression = "const val foo = 12"
        val snippetCount = 1
        val expected = """const val foo = com.pajato.edgar.collector.DataCollector.record(0) { 12 }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a variable initialization with a string, verify a correct result`() {
        val expression = """val foo = "12""""
        val snippetCount = 1
        val expected = """val foo = com.pajato.edgar.collector.DataCollector.record(0) { "12" }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a variable initialization with an if expression, verify a correct result`() {
        val expression = """val foo = if (true) "True" else "False""""
        val snippetCount = 3
        val expected = """val foo = if (com.pajato.edgar.collector.DataCollector.record(0) { true }) """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { "True" } else """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { "False" }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a variable with function initializer, verify a correct result`() {
        val expression = """val foo = readLine()"""
        val snippetCount = 1
        val expected = """val foo = com.pajato.edgar.collector.DataCollector.record(0) { readLine() }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a variable with no initializer, verify a correct result`() {
        val expression = """var varWithNoInitializer: String"""
        val snippetCount = 0
        val expected = """var varWithNoInitializer: String"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a variable with string initializer, verify a correct result`() {
        val expression = """var varWithStringInitializer: String = "xyzzy""""
        val snippetCount = 1
        val expected = """var varWithStringInitializer: String = """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { "xyzzy" }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When initializing a variable with a when expression, verify a correct result`() {
        val expression = """val foo: String = when { true -> "True"; else -> "False" }"""
        val snippetCount = 3
        val expected = """val foo: String = when { com.pajato.edgar.collector.DataCollector.record(0) { true } -> """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { "True" }; else -> """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { "False" } }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }
}
