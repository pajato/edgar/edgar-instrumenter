package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.edgar.instrumenter.SourceInstrumenter.getRegisteredClient
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
internal class SourceInstrumenterUnitTest : InstrumenterTestProfiler() {

    @Test fun `When using an invalid cache path, verify correct behavior`() {
        val spec = ServerSpec("TestLS", "kt", "xyzzy", "file:////xyzzy", "file:/noSuchDir")
        val diagnosticMessage = """Client with id: "${spec.id}" and cache path: "xyzzy" is not null!"""
        activateClient(spec, testRegistryPath, cachePath = "xyzzy")
        assertEquals(null, getRegisteredClient(spec), diagnosticMessage)
    }

    @Test fun `When invoking toMap() with a valid URI, verify correct behavior`() {
        val uri: String = this::class.java.classLoader.getResource(".lsp/servers")!!.toString()
        val map = uri.toMap("some diagnostic message")
        assertEquals(1, map.size)
    }

    @Test fun `When there are no servers specified, verify correct behavior`() {
        assertFailsWith<IllegalStateException>("No servers specified!") {
            SourceInstrumenter.doInstrumentation(listOf())
        }
    }

    @Test fun `When a server fails to start, verify correct behavior`() {
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        assertTrue(
            actual = SourceInstrumenter.doInstrumentation(listOf(spec)).isNotEmpty(),
            message = getDiagnostic("(empty) error message")
        )
    }

    @Test fun `When copy path creator fails, verify correct behavior`() {
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        CopyPathCreator.throwForTest = true
        assertTrue(doSourceCopy(spec).isNotEmpty(), getDiagnostic("(empty) error message"))
        CopyPathCreator.throwForTest = false
    }

    @Test fun `When server capabilities is null, verify correct behavior`() {
        val classLoader = this::class.java.classLoader
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")

        activateClient(spec, "invalid test registry path")
        assertEquals(null, getRegisteredClient(spec), "Expected null client with id: ${spec.id }!")
    }
}
