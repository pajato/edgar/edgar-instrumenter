package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbolOptions
import com.pajato.isaac.api.Id
import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.NotificationMessage
import com.pajato.isaac.api.OptionsDocumentSymbolProvider
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.core.CoreLogger
import com.pajato.isaac.core.Logger
import com.pajato.isaac.core.MessageRouter
import com.pajato.isaac.core.ResponseHandler

class TestMessageRouter : MessageRouter, Logger by CoreLogger() {
    private fun getCapabilities(): ServerCapabilities {
        return ServerCapabilities(documentSymbolProvider = OptionsDocumentSymbolProvider(DocumentSymbolOptions(true)))
    }

    override var initializeResult: InitializeResult? = InitializeResult(getCapabilities())
    override val responseHandlerMap: MutableMap<Int, (ResponseMessage) -> Unit>
        get() = TODO("Not yet implemented")

    override fun getNextRequestId(): Id {
        TODO("Not yet implemented")
    }

    override fun incrementNextRequestId() {
        TODO("Not yet implemented")
    }

    override fun logMessage(category: String, message: String, scope: String) {
        TODO("Not yet implemented")
    }

    override fun sendNotification(notification: NotificationMessage) {
        TODO("Not yet implemented")
    }

    override fun sendRequest(request: RequestMessage, responseHandler: ResponseHandler): Id {
        TODO("Not yet implemented")
    }

    override fun start(timeout: Long, vararg args: String) {
        TODO("Not yet implemented")
    }

    override fun stop(timeout: Long) {
        TODO("Not yet implemented")
    }

    override suspend fun waitForConditionOrTimeout(
        timeout: Long,
        id: Id,
        method: String,
        condition: () -> Boolean
    ): Boolean {
        TODO("Not yet implemented")
    }
}
