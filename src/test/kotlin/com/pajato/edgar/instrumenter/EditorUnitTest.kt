package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.Position
import com.pajato.isaac.core.CoreTestProfiler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalCoroutinesApi
class EditorUnitTest : CoreTestProfiler() {

    @Test fun `When an empty text file, verify result`() {
        val temp: File = kotlin.io.path.createTempFile().toFile().apply { deleteOnExit() }
        val editor: Editor = FileEditor(temp)
        val start = Position(1, 1)
        val end = Position(3, 4)
        assertEquals(-1, editor.convert(start), getDiagnostic("Invalid position conversion detected!"))
        assertEquals("", editor.getText(start, end), getDiagnostic("getText()"))
    }
}
