package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.Function
import com.pajato.isaac.core.CoreTestProfiler
import kotlin.io.path.createTempFile
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class WhenExpressionExtensionsUnitTest : CoreTestProfiler() {

    private val name = "name"
    private val range = Range()
    private val kid = DocumentSymbol(name, Function, detail = valueArgumentDetail, range = range, children = listOf())
    private val children: List<DocumentSymbol> = listOf(kid)
    private val editor: Editor = FileEditor(createTempFile().toFile().apply { deleteOnExit() })

    @Test fun registerWhenEntryChild() {
        val symbol = DocumentSymbol(name, Function, detail = valueArgumentDetail, range = range, children = children)
        assertTrue(symbol.getDiagnosticOrRegisterWhenEntryChild(editor).isNotEmpty())
    }

    @Test fun registerWhenConditionWithExpression() {
        val symbol = DocumentSymbol(name, Function, detail = valueArgumentDetail, range = range, children = children)
        assertTrue(symbol.registerWhenConditionWithExpression(editor).isNotEmpty())
    }

    @Test fun `When registering a when expression with an invalid detail, verify correct behavior`() {
        val expectedDiagnostic = getUnsupportedDetailMessage(noSuchDetail, whenExpressionDetail)
        val symbol = DocumentSymbol(name, Function, detail = noSuchDetail, range = range, children = children)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenExpression(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a when expression with null children, verify correct behavior`() {
        val expectedDiagnostic = getNullListMessage()
        val symbol = DocumentSymbol(name, Function, detail = whenExpressionDetail, range = range)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenExpression(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a when expression with no children, verify correct behavior`() {
        val expectedDiagnostic = getInvalidSizeMessage(whenExpressionDetail)
        val children = mutableListOf<DocumentSymbol>()
        val symbol = DocumentSymbol(name, Function, detail = whenExpressionDetail, range = range, children = children)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenExpression(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering when entries with an invalid detail, verify correct behavior`() {
        val expectedDiagnostic = getUnsupportedDetailMessage(noSuchDetail, whenEntryDetail)
        val symbol = DocumentSymbol(name, Function, detail = noSuchDetail, range = range)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntries(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering when entries with null children, verify correct behavior`() {
        val expectedDiagnostic = getNullListMessage()
        val symbol = DocumentSymbol(name, Function, detail = whenEntryDetail, range = range)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntries(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering when entries with no children, verify correct behavior`() {
        val expectedDiagnostic = getInvalidSizeMessage(whenEntryDetail)
        val symbol = DocumentSymbol(name, Function, detail = whenEntryDetail, range = range, children = mutableListOf())
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntries(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a when entry child with null children, verify correct behavior`() {
        val expectedDiagnostic = getNullListMessage()
        val symbol = DocumentSymbol(name, Function, detail = noSuchDetail, range = range)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntryChild(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a when entry child with no children, verify correct behavior`() {
        val expectedDiagnostic = getInvalidSizeMessage(0, 1)
        val symbol = DocumentSymbol(name, Function, detail = noSuchDetail, range = range, children = mutableListOf())
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntryChild(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a when entry child with an invalid detail, verify correct behavior`() {
        val choices = "$whenConditionWithExpression or $stringTemplateExpressionDetail"
        val expectedDiagnostic = getUnsupportedDetailMessage(noSuchDetail, choices)
        val symbol = DocumentSymbol(name, Function, detail = noSuchDetail, range = range, children = children)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterWhenEntryChild(editor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }
}
