package com.pajato.edgar.instrumenter

import kotlin.test.BeforeTest
import kotlin.test.Test

class BasicObjectDeclarationUnitTest : InstrumenterTestProfiler() {

    @BeforeTest fun setup() { activateClientWithCache(testServerSpec, testRegistryPath) }

    @Test fun `Given an object with no block, verify correct behavior`() {
        val expression = """object Main"""
        val snippetCount = 0
        val expected = """object Main"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given an object with an empty block, verify correct behavior`() {
        val expression = """object Main { }"""
        val snippetCount = 0
        val expected = """object Main { }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given an object with a single initialized property, verify correct behavior`() {
        val expression = """object Main {
            |    val prop = "Some property"
            |}
        """.trimMargin()
        val snippetCount = 1
        val expected = """object Main {
            |    val prop = com.pajato.edgar.collector.DataCollector.record(0) { "Some property" }
            |}
        """.trimMargin()
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }
}
