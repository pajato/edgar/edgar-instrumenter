package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Ignore
import kotlin.test.Test

class InstrumenterDogFoodTest : InstrumenterTestProfiler() {
    private val projectName = "edgar-instrumenter"
    private val projectUri = "file:${URI(System.getProperty("user.dir"))}"
    private val copyUri = "$projectUri$defaultBuildCopyDir"
    val serverSpec = ServerSpec(id, "kt", projectName, projectUri, copyUri)

    @BeforeTest fun setup() { activateClientWithCache(serverSpec, testRegistryPath) }

    @Ignore
    @Test fun `When instrumenting the class extensions file, verify behavior`() {
        val fileName = "ClassKindExtensions.kt"
        assertInstrumentationWithFileName(serverSpec, fileName)
    }
}
