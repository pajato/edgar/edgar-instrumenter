package com.pajato.edgar.instrumenter

import com.pajato.edgar.instrumenter.SourceInstrumenter.registeredClientMap
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.core.CoreTestProfiler
import com.pajato.isaac.core.LogEntry
import com.pajato.isaac.core.client.Client
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class PlumbingUnitTest : CoreTestProfiler() {

    @BeforeTest fun setup() { registeredClientMap.clear() }

    @Test fun `Ensure that the logger plumbing is in place`() {
        val entries: MutableList<LogEntry> = mutableListOf()
        client.router.subscribe { entries.add(it) }
        client.router.logMessage(clientInfoTag, "some message", this.javaClass.simpleName)
        assertEquals(1, entries.size)
    }

    @Test fun `Ensure that the registered servers plumbing is in place`() {
        assertEquals(0, registeredClientMap.size)
    }

    private val classLoader: ClassLoader = this::class.java.classLoader
    private val testServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
    private val client: Client = getClient(testServerSpec, testRegistryPath)
}
