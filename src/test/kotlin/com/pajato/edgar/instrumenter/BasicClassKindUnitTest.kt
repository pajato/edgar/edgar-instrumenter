package com.pajato.edgar.instrumenter

import kotlin.test.BeforeTest
import kotlin.test.Test

class BasicClassKindUnitTest : InstrumenterTestProfiler() {

    @BeforeTest fun setup() { activateClientWithCache(testServerSpec, testRegistryPath) }

    @Test fun `Given a class with no executable snippets, verify correct behavior`() {
        val expression = """class Main"""
        val snippetCount = 0
        val expected = """class Main"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with a default, no-arg constructor, verify correct behavior`() {
        val expression = """class Main()"""
        val snippetCount = 0
        val expected = """class Main()"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with a default, single arg constructor, verify correct behavior`() {
        val expression = """class Main(arg: String)"""
        val snippetCount = 0
        val expected = """class Main(arg: String)"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with no initialized properties, verify correct behavior`() {
        val expression = "class Main { }"
        val snippetCount = 0
        val expected = "class Main { }"
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with an initialized property, verify correct behavior`() {
        val expression = """class Main(val arg: Int = 23)"""
        val snippetCount = 1
        val expected = """class Main(val arg: Int = com.pajato.edgar.collector.DataCollector.record(0) { 23 })"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with a single initialized property, verify correct behavior`() {
        val expression = """class Main {
            |    val prop = "Some property"
            |}
        """.trimMargin()
        val snippetCount = 1
        val expected = """class Main {
            |    val prop = com.pajato.edgar.collector.DataCollector.record(0) { "Some property" }
            |}
        """.trimMargin()
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with a single method, verify correct behavior`() {
        val expression = """class Main {
            |    fun hw() { println("Hello World") }
            |}
        """.trimMargin()
        val snippetCount = 2
        val expected = """class Main {
            |    fun hw() { com.pajato.edgar.collector.DataCollector.record(0) { println(com.pajato.edgar.collector.DataCollector.record(0) { "Hello World" }) } }
            |}
        """.trimMargin()
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class with only an init method, verify correct behavior`() {
        val expression = """class Main {
            |    init { println("Hello World") }
            |}
        """.trimMargin()
        val snippetCount = 2
        val expected = """class Main {
            |    init { com.pajato.edgar.collector.DataCollector.record(0) { println(com.pajato.edgar.collector.DataCollector.record(0) { "Hello World" }) } }
            |}
        """.trimMargin()
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given an interface with no executable snippets, verify correct behavior`() {
        val expression = """interface Interface"""
        val snippetCount = 0
        val expected = """interface Interface"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `Given a class that implements a marker interface, verify correct behavior`() {
        val expression = """interface Marker
            |class Main : Marker {
            |}
        """.trimMargin()
        val snippetCount = 0
        val expected = """interface Marker
            |class Main : Marker {
            |}
        """.trimMargin()
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }
}
