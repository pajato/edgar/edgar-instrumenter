package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind
import com.pajato.isaac.core.CoreTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class PropertyKindExtensionsUnitTest : CoreTestProfiler() {

    @Test fun `When registering a property with an invalid detail, verify correct behavior`() {
        val symbol = DocumentSymbol(
            "xyzzy", SymbolKind.Property, null, null, "KtProperty", range, range,
            listOf(child, child)
        )
        val diagnostic = symbol.getDiagnosticOrRegisterPropertyKind(testEditor)
        assertTrue(diagnostic.isNotEmpty(), getDiagnostic("detail error"))
    }

    @Test fun `When registering a property with too few children, verify correct behavior`() {
        val symbol = DocumentSymbol("xyzzy", SymbolKind.File, null, null, "KtProperty", range, range, listOf())
        val diagnostic = symbol.getDiagnosticOrRegisterPropertyKind(testEditor)
        assertTrue(diagnostic.isNotEmpty(), getDiagnostic("detail error"))
    }

    @Test fun `When failing to register a constant expression to a list, verify correct behavior`() {
        val symbol = DocumentSymbol(name, SymbolKind.Function, detail = noSuchDetail, range = Range())
        val expectedDiagnostic = symbol.getRegisterFailedMessage()
        val actualDiagnostic = symbol.getDiagnosticOrRegisterConstantExpression(testEditor, TestList())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    private val name = "name"
    private val range = Range(Position(), Position())
    private val child = DocumentSymbol("xyzzy", SymbolKind.Function, null, null, "KtNoSuchDetail", range, range, null)
}
