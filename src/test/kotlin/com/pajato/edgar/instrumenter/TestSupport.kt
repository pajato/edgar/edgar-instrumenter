package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientInfo
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.api.Trace
import com.pajato.isaac.api.WorkspaceFolder
import com.pajato.isaac.core.client.Client
import com.pajato.isaac.core.client.defaultServerSpecPath
import com.pajato.isaac.core.testClassLoader
import java.io.File
import java.lang.management.ManagementFactory
import java.net.URI
import java.net.URL
import kotlin.io.path.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.createTempFile
import kotlin.io.path.toPath
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

internal const val defaultBuildCopyDir = "/build/coverage-copy/"
internal const val returnedDiagnostic = "returned diagnostic"
internal const val noSuchDetail = "noSuchDetail"

internal val testEditor: Editor = FileEditor(createTempFile().toFile().apply { deleteOnExit() })

internal fun getDiagnostic(snippet: String): String = "The $snippet is wrong!"

internal fun getExpectedSourceFile(filePath: String): File {
    val path = "dogFoodFiles/${Path(filePath).toFile().name}.exp"
    val url: URL = testClassLoader.getResource(path) ?: throw IllegalStateException("Invalid test file path: ($path)!")
    return url.toURI().toPath().toFile()
}

internal fun getCopySourceFile(spec: ServerSpec): File {
    val classLoader: ClassLoader = spec::class.java.classLoader
    val resourcePath = "sample_projects/kotlin_gradle/basics/src/main/kotlin/samples/basics.kt"
    val resourceUrl = classLoader.getResource(resourcePath) ?: fail("Invalid resource: $resourcePath")
    return resourceUrl.toURI().toPath().toFile()
}

internal fun assertInstrumentationWithFileName(projectSpec: ServerSpec, fileName: String) {
    val expectedFile: File = getExpectedSourceFile(fileName)
    val packagePath = "com/pajato/edgar/instrumenter"
    val sourceFile = File(URI(projectSpec.copyUri).toPath().toFile(), "/src/main/kotlin/$packagePath/$fileName")
    val map = SourceInstrumenter.doInstrumentation(listOf(projectSpec))
    val diagnostic = map[sourceFile.absolutePath] ?: ""
    assertEquals("", diagnostic, "Errors have been reported! Fix them to continue.")
    assertEquals(expectedFile.readText(), sourceFile.readText())
}

internal fun assertInstrumentationWithContent(projectSpec: ServerSpec, content: String, count: Int) {
    val testFilePath = "src/main/kotlin/samples/basics.kt"
    val dir = File(projectSpec.copyUri.substring("file:".length))
    val sourceFile = File(dir, testFilePath)
    val map = SourceInstrumenter.doInstrumentation(listOf(projectSpec))
    val diagnostic = map[sourceFile.absolutePath] ?: ""
    assertEquals("", diagnostic, "Errors have been reported! Fix them to continue.")
    assertEquals(content, sourceFile.readText())
    assertEquals(count, executionCountMap.size)
}

internal fun ClassLoader.getServerSpec(id: String, projectDir: String, projectName: String): ServerSpec {
    fun getTestProjectUri(path: String): DocumentUri {
        fun getUriOrError(resource: URL?, path: String): String = when {
            resource != null -> resource.toURI().toString()
            else -> """Error: The project with path "$path" is not supported (cannot be found)!"""
        }

        val resource = this.getResource(path)
        return getUriOrError(resource, path)
    }

    val projectCopyDir = defaultBuildCopyDir + projectDir
    val copyUri: DocumentUri = "file:${System.getProperty("user.dir")}$projectCopyDir/$projectName/"
    val projectUri = getTestProjectUri("sample_projects/$projectDir/$projectName")

    URI(copyUri).toPath().createDirectories()
    return ServerSpec(id, "kt", projectName, projectUri, copyUri)
}

internal fun activateClient(spec: ServerSpec, registryPath: String = "", cachePath: String = ""): Diagnostic {
    fun registerClient(): Diagnostic =
        getClient(spec, registryPath, cachePath).let { SourceInstrumenter.registeredClientMap[spec.id] = it; "" }

    return try { registerClient() } catch (exc: Exception) { getInvalidClientMessage(spec.id, exc) }
}

internal fun getClient(spec: ServerSpec, registryPath: String = "", cachePath: String = ""): Client =
    Client(spec.id, getInitializeParams(spec), registryPath.ifEmpty { defaultServerSpecPath }, cachePath)

internal fun getInitializeParams(spec: ServerSpec) = InitializeParams(
    trace = Trace.verbose,
    clientInfo = ClientInfo("Edgar", "tbd"), capabilities = ClientCapabilities(),
    processId = ManagementFactory.getRuntimeMXBean().name.split("@")[0].toInt(),
    workspaceFolders = listOf(WorkspaceFolder(spec.projectUri, spec.projectName))
)

internal fun getInvalidClientMessage(id: String, exc: Exception) =
    "$id is not a valid client id. Failed with exception message: ${exc.message}!"

class UnitTest {
    @Test fun `sanity test`() {
        assertEquals(4, 2 + 2)
    }
}
