package com.pajato.edgar.instrumenter

import kotlin.test.BeforeTest
import kotlin.test.Test

class BasicMapUnitTest : InstrumenterTestProfiler() {

    @BeforeTest fun setup() { activateClientWithCache(testServerSpec, testRegistryPath) }

    @Test fun `When declaring an empty map, verify correct behavior`() {
        val expression = "internal val dataMap: MutableMap<Int, Int> = mutableMapOf()"
        val snippetCount = 1
        val expected = "internal val dataMap: MutableMap<Int, Int> = " +
            "com.pajato.edgar.collector.DataCollector.record(0) { mutableMapOf() }"
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When modifying an empty map with a constant, verify correct behavior`() {
        val expression = """internal val dataMap: MutableMap<Int, Int> = mutableMapOf()
            internal fun assignToMap() { dataMap[0] = 1 }"""
        val snippetCount = 4
        val expected = """internal val dataMap: MutableMap<Int, Int> = """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { mutableMapOf() }
            internal fun assignToMap() { com.pajato.edgar.collector.DataCollector.record(0) { dataMap[""" +
            """ com.pajato.edgar.collector.DataCollector.record(0) { 0 } ] = """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { 1 } } }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When modifying an empty map with an identifier, verify correct behavior`() {
        val expression = """internal val dataMap: MutableMap<Int, Int> = mutableMapOf()
            internal val id = 3
            internal fun assignToMap() { dataMap[0] = id }"""
        val snippetCount = 5
        val expected = """internal val dataMap: MutableMap<Int, Int> = """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { mutableMapOf() }
            internal val id = com.pajato.edgar.collector.DataCollector.record(0) { 3 }
            internal fun assignToMap() { com.pajato.edgar.collector.DataCollector.record(0) { dataMap[""" +
            """ com.pajato.edgar.collector.DataCollector.record(0) { 0 } ] = """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { id } } }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }
}
