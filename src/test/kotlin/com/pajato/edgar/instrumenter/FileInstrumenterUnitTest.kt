package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.CoreTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

internal class FileInstrumenterUnitTest : CoreTestProfiler() {

    @Test fun `When the registered client map is corrupted, verify the correct file instrumenter behavior`() {
        val classLoader = this::class.java.classLoader
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        SourceInstrumenter.registeredClientMap.clear()
        assertEquals(getNoClientErrorMessage(spec.id), FileInstrumenter.doOperation(spec), getDiagnostic("diagnostic"))
    }
}
