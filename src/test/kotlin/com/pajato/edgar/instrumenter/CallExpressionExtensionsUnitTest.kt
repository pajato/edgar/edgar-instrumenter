package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.Function
import kotlin.test.Test
import kotlin.test.assertEquals

class CallExpressionExtensionsUnitTest : InstrumenterTestProfiler() {

    @Test fun `When registering a call expression with null children, verify correct behavior`() {
        val expectedDiagnostic = getNullListMessage()
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range())
        val actualDiagnostic = symbol.getDiagnosticOrRegisterCallExpression(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering a call expression with too many children, verify correct behavior`() {
        val expectedDiagnostic = getInvalidSizeMessage(3, 2)
        val child = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range())
        val kids = mutableListOf(child, child, child)
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range(), children = kids)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterCallExpression(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering a call expression with an invalid first child, verify correct behavior`() {
        val expectedDiagnostic = getInvalidCallExpressionNameMessage()
        val child = DocumentSymbol(name, Function, detail = noSuchDetail, range = Range())
        val kids = mutableListOf(child, child)
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range(), children = kids)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterCallExpression(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering a call expression with an invalid second child, verify correct behavior`() {
        val expectedDiagnostic = getInvalidCallExpressionArgListMessage()
        val child1 = DocumentSymbol(name, Function, detail = nameReferenceExpressionDetail, range = Range())
        val child2 = DocumentSymbol(name, Function, detail = noSuchDetail, range = Range())
        val kids = mutableListOf(child1, child2)
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range(), children = kids)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterCallExpression(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering a call expression value argument list with no children, verify correct behavior`() {
        val expectedDiagnostic = getInvalidSizeMessage(0, 2)
        val kids = mutableListOf<DocumentSymbol>()
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range(), children = kids)
        val actualDiagnostic =
            symbol.children!!.getDiagnosticOrRegisterCallExpressionValueArgumentList(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering a call expression value argument list with null children, verify correct behavior`() {
        val expectedDiagnostic = getNullListMessage()
        val symbol = DocumentSymbol(name, Function, detail = callExpressionDetail, range = Range())
        val actualDiagnostic = symbol.getDiagnosticOrRegisterValueArgumentList(editor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("diagnostic"))
    }

    private val name = "name"
    private val editor: Editor = testEditor
}
