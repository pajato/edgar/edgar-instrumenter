package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.File
import kotlin.test.Test
import kotlin.test.assertEquals

class ObjectExtensionsUnitTest {

    @Test fun `Force an unsupported detail error when registering a child of an object declaration`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("symbol", File, detail = noSuchDetail, range = range)
        val diagnostic = getUnsupportedDetailMessage(symbol.detail, classBodyDetail)
        assertEquals(
            expected = diagnostic,
            actual = symbol.getDiagnosticOrRegisterObjectDeclarationChild(testEditor),
            message = getDiagnostic(returnedDiagnostic)
        )
    }
}
