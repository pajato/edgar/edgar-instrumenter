package com.pajato.edgar.instrumenter

import com.pajato.edgar.collector.DataCollector
import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.CoreTestProfiler
import com.pajato.isaac.core.MessageCache
import java.io.File
import kotlin.test.DefaultAsserter.fail

const val fwcdKLS = "KLS@fwcd1.2.0"

abstract class InstrumenterTestProfiler : CoreTestProfiler() {
    internal val id = fwcdKLS
    internal val classLoader: ClassLoader = this::class.java.classLoader
    internal val testServerSpec = classLoader.getServerSpec(id, "kotlin_gradle", "basics")
    internal val sourceFile: File by lazy { getCopySourceFile(testServerSpec) }

    private fun getCachePath(key: String) = "${System.getProperty("user.home")}/.lsp/${key.hashCode()}.cache"

    fun activateClientWithCache(spec: ServerSpec, registryPath: String) {
        val cachePath = getCachePath(getKey())

        fun setupToFillCacheMaybe() {
            val client = SourceInstrumenter.registeredClientMap[id] ?: fail("Client with id: $id is not registered!")

            fun setupCache() {
                val cache = MessageCache(client)
                client.logger.subscribe { cache.addEntryToCache(it) }
            }

            setLogMap(client)
            if (client.cachePath.isNotEmpty() && File(client.cachePath).length() == 0L) { setupCache() }
        }

        SourceInstrumenter.registeredClientMap.clear()
        DataCollector.clear()
        activateClient(spec, registryPath, cachePath).also { if (it.isEmpty()) setupToFillCacheMaybe() }
    }
}
