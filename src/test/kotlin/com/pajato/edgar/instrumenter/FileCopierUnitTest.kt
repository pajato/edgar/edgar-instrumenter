package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.CoreTestProfiler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

@ExperimentalCoroutinesApi
internal class FileCopierUnitTest : CoreTestProfiler() {
    private val dirUri: String = this::class.java.classLoader.getResource(".lsp")?.toString()
        ?: fail("Valid file resource not found!")
    private val fileUri: String = this::class.java.classLoader.getResource("aValidFile.txt")?.toString()
        ?: fail("Valid file resource not found!")

    @Test fun `When validating an invalid uri, verify correct behavior`() {
        assertEquals(null, getFileOrNull(uri = "< >"), getDiagnostic("getFileOrNull"))
    }

    @Test fun `When forcing errors on doCopy() with invalid arguments, verify correct behavior`() {
        val sourceFile = File("/no_such_dir/no_such_file")
        val targetDir = File("another non existing file")
        val copyError = sourceFile.doCopy(targetDir)
        assertTrue(copyError.isNotEmpty(), getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the source (projectUri) is not a valid file, verify the correct behavior`() {
        val invalidUri = "jar://xyzzy"
        val spec = ServerSpec("id", "extension", "projectName", invalidUri, "file://copyUri", listOf())
        val diagnostic = FileCopier.doOperation(spec)
        assertEquals(invalidProjectUriErrorMessage, diagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When the target (copyUri) is not a valid file, verify the correct behavior`() {
        val invalidUri = "jar://xyzzy"
        val spec = ServerSpec("id", "extension", "projectName", "file://copyUri", invalidUri, listOf())
        val diagnostic = FileCopier.doOperation(spec)
        assertEquals(invalidProjectUriErrorMessage, diagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When the source uri is a file, verify correct behavior`() {
        val spec = ServerSpec("id", "extension", "projectName", fileUri, dirUri, listOf())
        val diagnostic = FileCopier.doOperation(spec)
        assertEquals(sourceIsFileError, diagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When the target uri is a file, verify correct behavior`() {
        val spec = ServerSpec("id", "extension", "projectName", dirUri, fileUri, listOf())
        val diagnostic = FileCopier.doOperation(spec)
        assertEquals(targetIsFileError, diagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When the source and target uris are the same, verify correct behavior`() {
        val spec = ServerSpec("id", "extension", "projectName", dirUri, dirUri, listOf())
        val diagnostic = FileCopier.doOperation(spec)
        assertEquals(overwriteError, diagnostic, getDiagnostic("diagnostic"))
    }

    @Test fun `When the source and target uris exist, are valid, and non-overlapping, verify the correct behavior`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        CopyFileDeleter.doOperation(spec)
        assertEquals("", FileCopier.doOperation(spec), getDiagnostic("pre-rename"))
        assertEquals(renameError, FileCopier.doOperation(spec), getDiagnostic("rename"))
    }

    @Test fun `When the source and target uris are valid and non-overlapping, verify the correct behavior`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val spec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        val deleteDiagnostic = CopyFileDeleter.doOperation(spec)
        val copyDiagnostic = FileCopier.doOperation(spec)
        assertEquals("", deleteDiagnostic, getDiagnostic("delete diagnostic"))
        assertEquals("", copyDiagnostic, getDiagnostic("copy diagnostic"))
    }
}
