package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.testClassLoader
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

private const val copyPathCreatorDiagnostic = "copy path creator diagnostic"

class CopyPathCreatorUnitTest {

    @BeforeTest fun setUp() { CopyPathCreator.throwForTest = true }
    @AfterTest fun tearDown() { CopyPathCreator.throwForTest = false }

    @Test fun `Force a non-empty diagnostic`() {
        val spec: ServerSpec = testClassLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        assertTrue(CopyPathCreator.hasDiagnostic(spec), getDiagnostic(copyPathCreatorDiagnostic))
    }
}
