package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.Diagnostic
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.File
import com.pajato.isaac.api.SymbolKind.Function
import kotlin.io.path.createTempFile
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class FileKindExtensionsUnitTest : InstrumenterTestProfiler() {

    @Test fun `When an invalid top level (file kind) detail is presented, verify correct behavior`() {
        val range = Range(Position(), Position())
        val child = DocumentSymbol("xyzzy", Function, detail = noSuchDetail, range = range)
        val symbol = DocumentSymbol("xyzzy", File, detail = fileDetail, range = range, children = listOf(child))
        val diagnostic = symbol.getDiagnosticOrInstrumentTopLevelSymbols(editor)
        assertTrue(diagnostic.isNotEmpty(), getDiagnostic("detail error"))
    }

    @Test fun `Verify that a function kind with no children behaves correctly`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Function, detail = fileDetail, range = range, children = listOf())
        val expectedDiagnostic = getInvalidSizeMessage(symbol.detail)
        val actualDiagnostic = symbol.getDiagnosticOrInstrumentTopLevelSymbols(testEditor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("detail error"))
    }

    @Test fun `Force a null list error on a child collection and verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("symbol", File, detail = fileDetail, range = range, children = null)
        val diagnostic = getNullListMessage()
        val errorMessage = "Internal software error: symbol with detail: ${symbol.detail} should have not got here!"

        fun test(): Diagnostic = symbol.children.getDiagnosticOrRegister { fail(errorMessage) }

        assertEquals(diagnostic, test(), getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When the symbol children size is larger than allowed, verify correct behavior`() {
        val range = Range(Position(), Position())
        val child = DocumentSymbol("xyzzy", Function, detail = "noSuchDetail", range = range)
        val kids = listOf(child, child, child, child)
        val symbol = DocumentSymbol("symbol", File, detail = fileDetail, range = range, children = kids)
        val diagnostic = getInvalidSizeMessage(4, 2, 3)
        val actualDiagnostic = symbol.getDiagnostic(2, 3, fileDetail)
        assertEquals(diagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    private val editor: Editor = FileEditor(file = createTempFile().toFile().apply { deleteOnExit() })
}
