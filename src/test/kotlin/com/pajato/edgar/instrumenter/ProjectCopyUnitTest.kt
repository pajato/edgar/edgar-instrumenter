package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.core.CoreTestProfiler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import java.net.URL
import java.nio.file.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.exists
import kotlin.io.path.fileSize
import kotlin.io.path.isDirectory
import kotlin.io.path.isRegularFile
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.io.path.Path as KPath

@ExperimentalCoroutinesApi
class ProjectCopyUnitTest : CoreTestProfiler() {

    @BeforeTest fun setup() {
        val defaultCopyPath: Path = KPath("${System.getProperty("user.dir")}/$defaultBuildCopyDir")
        if (!defaultCopyPath.exists()) defaultCopyPath.createDirectory()
        if (defaultCopyPath.listDirectoryEntries().isEmpty()) return
        defaultCopyPath.listDirectoryEntries().forEach { path -> path.toFile().deleteRecursively() }
        assert(defaultCopyPath.exists() && defaultCopyPath.isDirectory() && defaultCopyPath.listDirectoryEntries().isEmpty())
    }

    @Test fun `When the project uri is invalid, verify the diagnostic`() {
        val projectUri: DocumentUri = "file:///xyzzy"
        val spec = ServerSpec("xyzzy", "kt", "testProject", projectUri, projectUri)
        val copyError = doSourceCopy(spec)
        assertEquals(noSourceError, copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the copy uri is invalid, verify the diagnostic`() {
        val projectUri: DocumentUri = "file://${this::class.java.classLoader.getResource("aValidFile.txt")!!.file}"
        val copyUri: DocumentUri = "file:///xyzzy"
        val spec = ServerSpec("xyzzy", "kt", "testProject", projectUri, copyUri)
        val copyError = doSourceCopy(spec)
        assertEquals(noCopyError, copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the copy uri is not a file, verify the diagnostic`() {
        val projectUri: DocumentUri = "file://${this::class.java.classLoader.getResource("aValidFile.txt")!!.file}"
        val copyUri: DocumentUri = "jar:///xyzzy"
        val spec = ServerSpec("xyzzy", "kt", "testProject", projectUri, copyUri)
        assertTrue(FileCopier.hasDiagnostic(spec))
        assertEquals(invalidCopyUriErrorMessage, FileCopier.errorDiagnostic, getDiagnostic("file copier diagnostic"))
    }

    @Test fun `When the project uri is a non-directory, verify the diagnostic`() {
        val projectUri: DocumentUri = "file://${this::class.java.classLoader.getResource("aValidFile.txt")!!.file}"
        val spec = ServerSpec("xyzzy", "kt", "testProject", projectUri, projectUri)
        val copyError = doSourceCopy(spec)
        assertEquals(sourceIsFileError, copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the copy uri is a non-directory, verify the diagnostic`() {
        val dir = "sample_projects/kotlin_gradle/basics"
        val projectUri: DocumentUri = "file://${this::class.java.classLoader.getResource(dir)!!.file}"
        val copyUri: DocumentUri = "file://${this::class.java.classLoader.getResource("aValidFile.txt")!!.file}"
        val spec = ServerSpec("xyzzy", "kt", "testProject", projectUri, copyUri)
        val copyError = doSourceCopy(spec)
        assertEquals(targetIsFileError, copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the destination and source paths are identical, verify a correct diagnostic`() {
        val dir = "sample_projects/kotlin_gradle/basics"
        val projectUri: DocumentUri = "file://${this::class.java.classLoader.getResource(dir)!!.file}"
        val copyUri: DocumentUri = "file://${this::class.java.classLoader.getResource(dir)!!.file}"
        val spec = ServerSpec(fwcdKLS, "kt", "instrumenter", projectUri, copyUri)
        val copyError = doSourceCopy(spec)
        assertEquals(overwriteError, copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the destination path is empty, verify a correct diagnostic`() {
        val spec: ServerSpec = this::class.java.classLoader.getServerSpec("KLS", "kotlin_gradle", "basics")
        val copyError = doSourceCopy(spec)
        assertEquals("", copyError, getDiagnostic("project delete diagnostic"))
    }

    @Test fun `When the destination path is not empty, verify a correct diagnostic`() {
        fun assertCopyCompressed(spec: ServerSpec) {
            val compressionError = doSourceCopy(spec)
            assertEquals("", compressionError, getDiagnostic("project delete diagnostic"))
        }

        val spec: ServerSpec = this::class.java.classLoader.getServerSpec("KLS", "kotlin_gradle", "basics")
        val copyError = doSourceCopy(spec)

        assertEquals("", copyError, getDiagnostic("project delete diagnostic"))
        assertCopyCompressed(spec)
    }

    @Test fun `When the hello world Gradle project is copied to the destination folder, verify the result`() {
        fun assertCopy(sourcePath: Path, copyPath: Path) {
            assertTrue(sourcePath != copyPath)
            assertEquals("", compareFiles(sourcePath, copyPath))
        }

        val spec: ServerSpec = this::class.java.classLoader.getServerSpec("KLS", "kotlin_gradle", "basics")
        val copyError = doSourceCopy(spec)

        assertEquals("", copyError, getDiagnostic("project copy"))
        assertCopy(URL(spec.projectUri).toURI().toPath(), URL(spec.copyUri).toURI().toPath())
    }
}

internal fun compareFiles(sourceParent: Path, copyParent: Path): String {
    val names = sourceParent.listDirectoryEntries().map { it.name }.filter { it != "." && it != ".." }
    for (name: String in names) {
        val diagnostic = name.isNotEqualInParents(sourceParent, copyParent)
        if (diagnostic.isNotEmpty()) return diagnostic
    }
    return ""
}

internal fun String.isNotEqualInParents(sourceParent: Path, copyParent: Path): String {
    fun isFileDir(source: Path, copy: Path) = source.isRegularFile() && copy.isDirectory()
    fun isDirFile(source: Path, copy: Path) = source.isDirectory() && copy.isRegularFile()
    fun isFileFile(source: Path, copy: Path) = source.isRegularFile() && copy.isRegularFile()
    fun isDirDir(source: Path, copy: Path) = source.isDirectory() && copy.isDirectory()
    fun getSizeErrorMessage(source: Long, copy: Long) = """File "$this" has different sizes: ($source vs $copy)"""
    fun getSizeDiagnostic(source: Long, copy: Long) = if (source == copy) "" else getSizeErrorMessage(source, copy)
    fun getDirCompareDiagnostic(source: Path, copy: Path) = compareFiles(source, copy)

    val source = File(sourceParent.toFile(), this).toPath()
    val copy = File(copyParent.toFile(), this).toPath()

    return when {
        !copyParent.exists() -> "The copy directory ${copyParent.toAbsolutePath()} does not exist!"
        !source.exists() -> """The source path "${source.toAbsolutePath()}" does not exist!"""
        !copy.exists() -> """The copy path "${copy.toAbsolutePath()}" does nto exist!"""
        isFileDir(source, copy) -> """Source path "${source.name}" is a file and copy path is a directory!"""
        isDirFile(source, copy) -> """Source path "${source.name}"" is a directory and copy path is a file!"""
        isFileFile(source, copy) -> getSizeDiagnostic(source.fileSize(), copy.fileSize())
        isDirDir(source, copy) -> getDirCompareDiagnostic(source, copy)
        else -> "Unexpected file type: only regular files and directories are supported!"
    }
}
