package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.File
import com.pajato.isaac.api.SymbolKind.Function
import kotlin.io.path.createTempFile
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import com.pajato.isaac.api.DocumentSymbol as Symbol

class FunctionKindExtensionsUnitTest {

    private val editor: Editor = FileEditor(file = createTempFile().toFile().apply { deleteOnExit() })
    private val name = "main"
    private val kids: Children = listOf()
    private val symbol = Symbol(name, Function, detail = valueArgumentDetail, range = Range(), children = kids)

    @Test fun `When registering function kind edits with invalid size, verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = Symbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expected = symbol.getDiagnosticOrRegisterFunctionKind(testEditor)
        assertTrue(expected.isNotEmpty(), getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail diagnostic`() {
        val range = Range(Position(), Position())
        val symbol = Symbol("foo", File, detail = noSuchDetail, range = range, children = listOf())
        val expected = symbol.getDiagnosticOrRegisterFunctionChild(testEditor)
        assertTrue(expected.isNotEmpty(), getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When an invalid value argument symbol is registered, verify correct behavior`() {
        val expected = getInvalidSizeMessage(0, 1)
        val actual = symbol.getDiagnosticOrRegisterValueArgument(editor, mutableListOf())
        assertEquals(expected, actual, getDiagnostic("diagnostic"))
    }

    @Test fun `When an unsupported value argument child detail is registered, verify correct behavior`() {
        val range = Range(Position(), Position())
        val child = Symbol(name, File, detail = noSuchDetail, range = range)
        val symbol = Symbol(name, File, detail = valueArgumentDetail, range = range, children = listOf(child))
        val expected = getUnsupportedDetailMessage(child.detail, valueArgumentChoiceMap.keys)
        val actual = symbol.getDiagnosticOrRegisterValueArgument(editor, mutableListOf())
        assertEquals(expected, actual, getDiagnostic("diagnostic"))
    }

    @Test fun `When a name reference value argument child detail is registered, verify correct behavior`() {
        val range = Range(Position(), Position())
        val child = Symbol(name, File, detail = nameReferenceExpressionDetail, range = range)
        val symbol = Symbol(name, File, detail = valueArgumentDetail, range = range, children = listOf(child))
        val expected = ""
        val actual = symbol.getDiagnosticOrRegisterValueArgument(editor, mutableListOf())
        assertEquals(expected, actual, getDiagnostic("diagnostic"))
    }

    @Test fun `When registering statement edits with other than a call expression detail, verify correct behavior`() {
        assertTrue(symbol.getDiagnosticOrRegisterStatementEdits(editor).isNotEmpty())
    }

    @Test fun `Force a null list error when registering a block`() {
        val range = Range(Position(), Position())
        val symbol = Symbol(name, File, detail = classInitializerDetail, range = range)
        val expected = getNullListMessage()
        val actual = symbol.getDiagnosticOrRegisterBlockExpression(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an unsupported detail error when registering an if expression`() {
        val range = Range(Position(), Position())
        val symbol = Symbol("symbol", File, detail = noSuchDetail, range = range)
        val expected = getUnsupportedDetailMessage(symbol.detail, ifExpressionDetail)
        val actual = symbol.getDiagnosticOrRegisterIfExpression(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null list error when registering an if expression`() {
        val range = Range(Position(), Position())
        val symbol = Symbol(name, File, detail = ifExpressionDetail, range = range)
        val expected = getNullListMessage()
        val actual = symbol.getDiagnosticOrRegisterIfExpression(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid size error when registering an if expression with no children`() {
        val range = Range(Position(), Position())
        val symbol = Symbol(name, File, detail = ifExpressionDetail, range = range, children = listOf())
        val expected = getInvalidSizeMessage(0, 3)
        val actual = symbol.getDiagnosticOrRegisterIfExpression(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null detail error when registering an element`() {
        val range = Range(Position(), Position())
        val symbol = Symbol(name, File, detail = null, range = range)
        val expected = getUnsupportedDetailMessage(symbol.detail, elementChoiceMap.keys)
        val actual = symbol.getDiagnosticOrRegisterElement(editor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }
}
