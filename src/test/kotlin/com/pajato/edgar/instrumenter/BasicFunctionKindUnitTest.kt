package com.pajato.edgar.instrumenter

import kotlin.test.BeforeTest
import kotlin.test.Test

class BasicFunctionKindUnitTest : InstrumenterTestProfiler() {

    @BeforeTest fun setup() { activateClientWithCache(testServerSpec, testRegistryPath) }

    @Test fun `When executing hello world, verify a correct result`() {
        val expression = """fun main() { println("Hello World") }"""
        val snippetCount = 2
        val expected = """fun main() { com.pajato.edgar.collector.DataCollector.record(0) """ +
            """{ println(com.pajato.edgar.collector.DataCollector.record(0) { "Hello World" }) } }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a private hello world, verify a correct result`() {
        val expression = """private fun main() { println("Hello World") }"""
        val snippetCount = 2
        val expected = """private fun main() { com.pajato.edgar.collector.DataCollector.record(0) """ +
            """{ println(com.pajato.edgar.collector.DataCollector.record(0) { "Hello World" }) } }"""
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a dot qualified statement, verify a correct result`() {
        val expression = """private fun main() { "Hello World".toUpperCase() }"""
        val expected = """private fun main() { com.pajato.edgar.collector.DataCollector.record(0) """ +
            """{ "Hello World".toUpperCase() } }"""
        val snippetCount = 1
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a dot qualified hello world, verify a correct result`() {
        val expression = """private fun main() { println("Hello World".toUpperCase()) }"""
        val expected = """private fun main() { com.pajato.edgar.collector.DataCollector.record(0) """ +
            """{ println(com.pajato.edgar.collector.DataCollector.record(0) { "Hello World".toUpperCase() }) } }"""
        val snippetCount = 2
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }

    @Test fun `When executing a postfix operation, verify a correct result`() {
        val expression = """private fun postFixTest(arg: Int) { arg++ }"""
        val expected = """private fun postFixTest(arg: Int) { """ +
            """com.pajato.edgar.collector.DataCollector.record(0) { arg++ } }"""
        val snippetCount = 1
        sourceFile.writeText(expression)
        assertInstrumentationWithContent(testServerSpec, expected, snippetCount)
    }
}
