package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind.Class
import com.pajato.isaac.api.SymbolKind.Property
import kotlin.test.Test
import kotlin.test.assertEquals

class ParameterExtensionsUnitTest : InstrumenterTestProfiler() {

    private val name = "name"

    @Test fun `Force an unsupported detail diagnostic when registering a parameter list symbol`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = "noSuchDetail", children = listOf(), range = range)
        val expected = getUnsupportedDetailMessage(symbol.detail, parameterListDetail)
        val actual = symbol.getDiagnosticOrRegisterParameterList(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null list diagnostic when registering parameters`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = null, range = range)
        val expected = getNullListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterParameters(testEditor)
        assertEquals(expected = expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a all list error diagnostic when registering parameters`() {
        val range = Range(Position(), Position())
        val child = DocumentSymbol("child", Property, detail = "noSuchDetail", range = range)
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(child), range = range)
        val expected = getAllMessage(parameterDetail)
        val actual = symbol.children.getDiagnosticOrRegisterParameters(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid list diagnostic`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(), range = range)
        val expected = getInvalidListMessage()
        val actual = symbol.getDiagnosticOrRegisterParameter(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an invalid child diagnostic`() {
        val range = Range(Position(), Position())
        val child = DocumentSymbol("child", Property, detail = "noSuchDetail", range = range)
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(child), range = range)
        val expected = getUnsupportedDetailMessage(child.detail, parameterChildChoices)
        val actual = symbol.getDiagnosticOrRegisterParameter(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force a null list diagnostic when registering all parameters`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = null, range = range)
        val expected = getNullListMessage()
        val actual = symbol.children.getDiagnosticOrRegisterAllParameters(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `Force an non-empty diagnostic when registering all parameters`() {
        val range = Range(Position(), Position())
        val grandChild = DocumentSymbol("grandChild", Property, detail = "noSuchDetail", range = range)
        val grandChildren = listOf(grandChild)
        val child = DocumentSymbol(name, Property, detail = "noSuchDetail", children = grandChildren, range = range)
        val symbol = DocumentSymbol("foo", Class, detail = classDetail, children = listOf(child), range = range)
        val expected = getUnsupportedDetailMessage(noSuchDetail, parameterChildChoices)
        val actual = symbol.children.getDiagnosticOrRegisterAllParameters(testEditor)
        assertEquals(expected, actual, getDiagnostic(returnedDiagnostic))
    }
}
