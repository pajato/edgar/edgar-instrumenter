package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind
import com.pajato.isaac.api.SymbolKind.File
import kotlin.test.Test
import kotlin.test.assertEquals

class BinaryExpressionExtensionsUnitTest : InstrumenterTestProfiler() {

    @Test fun `When registering a binary expression with null children, verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range)
        val expectedDiagnostic = getNullListMessage()
        val actualDiagnostic = symbol.getDiagnosticOrRegisterBinaryExpression(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a default binary expression with null children, verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range)
        val expectedDiagnostic = getNullListMessage()
        val actualDiagnostic = symbol.getDiagnosticOrRegisterBinaryExpression(testEditor)
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering no binary expression children, verify correct behavior`() {
        val kids: MutableList<DocumentSymbol> = mutableListOf()
        val expectedDiagnostic = getInvalidSizeMessage(0, 3)
        val actualDiagnostic = kids.getDiagnosticOrRegisterBinaryExpressionChildren(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a left hand side with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, arrayAccessExpressionDetail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterLeftHandSide(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering an operation reference expression with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, operationReferenceExpressionDetail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterOperation(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a right hand side with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, rightHandSideChoices)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterRightHandSide(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a name reference expression with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, nameReferenceExpressionDetail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterNameReferenceExpression(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When using an invalid detail to register a symbol on a list, verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getListRegisterErrorMessage(symbol.detail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterSymbolOnList(testEditor, TestList())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a container node with null children, verify the behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range)
        val expectedDiagnostic = getNullListMessage()
        val actualDiagnostic = symbol.getDiagnosticOrRegisterContainerNode(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering no container node children, verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf())
        val expectedDiagnostic = getInvalidSizeMessage(0, 1)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterContainerNode(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a container node with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val kid = DocumentSymbol("child", SymbolKind.Array, detail = "noSuchDetail", range = range)
        val symbol = DocumentSymbol("foo", File, detail = functionDetail, range = range, children = listOf(kid))
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, containerNodeDetail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterContainerNode(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering an array access expression with no children, verify correct behavior`() {
        val name = "name"
        val range = Range(Position(), Position())
        val kids = mutableListOf<DocumentSymbol>()
        val symbol = DocumentSymbol(name, File, detail = arrayAccessExpressionDetail, range = range, children = kids)
        val expectedDiagnostic = getInvalidSizeMessage(0, 2)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterArrayAccessExpression(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }

    @Test fun `When registering a container node child with an invalid detail verify correct behavior`() {
        val range = Range(Position(), Position())
        val symbol = DocumentSymbol("foo", File, detail = noSuchDetail, range = range)
        val expectedDiagnostic = getUnsupportedDetailMessage(symbol.detail, containerNodeChildChoices)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterContainerNodeChild(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic(returnedDiagnostic))
    }
}

internal class TestList : MutableList<String> {
    override val size: Int
        get() = TODO("Not yet implemented")

    override fun contains(element: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun containsAll(elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun get(index: Int): String {
        TODO("Not yet implemented")
    }

    override fun indexOf(element: String): Int {
        TODO("Not yet implemented")
    }

    override fun isEmpty(): Boolean {
        TODO("Not yet implemented")
    }

    override fun iterator(): MutableIterator<String> {
        TODO("Not yet implemented")
    }

    override fun lastIndexOf(element: String): Int {
        TODO("Not yet implemented")
    }

    override fun add(element: String): Boolean = false

    override fun add(index: Int, element: String) {
        TODO("Not yet implemented")
    }

    override fun addAll(index: Int, elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun addAll(elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun clear() {
        TODO("Not yet implemented")
    }

    override fun listIterator(): MutableListIterator<String> {
        TODO("Not yet implemented")
    }

    override fun listIterator(index: Int): MutableListIterator<String> {
        TODO("Not yet implemented")
    }

    override fun remove(element: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun removeAll(elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun removeAt(index: Int): String {
        TODO("Not yet implemented")
    }

    override fun retainAll(elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun set(index: Int, element: String): String {
        TODO("Not yet implemented")
    }

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<String> {
        TODO("Not yet implemented")
    }
}
