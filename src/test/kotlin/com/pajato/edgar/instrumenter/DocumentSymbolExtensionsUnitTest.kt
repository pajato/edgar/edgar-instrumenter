package com.pajato.edgar.instrumenter

import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind
import com.pajato.isaac.core.CoreTestProfiler
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentSymbolExtensionsUnitTest : CoreTestProfiler() {

    private val nullMessage = "Unsupported element with detail (null),"

    @Test fun `When registering statement edits with an invalid detail, verify correct behavior`() {
        val range = Range(Position(), Position())
        val child = DocumentSymbol("xyzzy", SymbolKind.Function, detail = noSuchDetail, range = range)
        val kids = listOf(child)
        val symbol = DocumentSymbol("foo", SymbolKind.File, detail = null, range = range, children = kids)
        val diagnostic = symbol.getDiagnosticOrRegisterStatementEdits(testEditor)
        assertTrue(diagnostic.isNotEmpty(), getDiagnostic("detail error"))
    }

    @Test fun `When registering statement edits with a null detail, verify correct behavior`() {
        val name = "symbol"
        val range = Range(Position(), Position())
        val kind = SymbolKind.File
        val symbol = DocumentSymbol(name, kind, detail = null, range = range, children = listOf())
        val expectedDiagnostic = getInvalidDetailMessage(null, valueArgumentDetail)
        val actualDiagnostic = symbol.getDiagnosticOrRegisterValueArgument(testEditor, mutableListOf())
        assertEquals(expectedDiagnostic, actualDiagnostic, getDiagnostic("detail error"))
    }

    @Test fun `When registering property kind edits for a document symbol with null children, verify behavior`() {
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val symbol = DocumentSymbol("test", SymbolKind.Property, range = Range(Position(0, 0)))
        val diagnostic = symbol.getDiagnosticOrRegisterPropertyKind(editor)
        assertEquals(diagnostic, getNullListMessage())
    }

    @Test fun `When registering property kind edits for a document symbol with non-null children, verify behavior`() {
        val name = "aName"
        val kind = SymbolKind.Property
        val range = Range(Position(0, 0))
        val editor: Editor = FileEditor(File.createTempFile("foo", "fie"))
        val child = DocumentSymbol(name, kind, range = range, detail = null)
        val symbol = DocumentSymbol(name, kind, detail = propertyDetail, range = range, children = listOf(child, child))
        assertTrue(symbol.getDiagnosticOrRegisterPropertyKind(editor).startsWith(nullMessage))
    }
}
