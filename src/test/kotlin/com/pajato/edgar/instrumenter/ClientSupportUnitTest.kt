package com.pajato.edgar.instrumenter

import com.pajato.edgar.core.config.ServerSpec
import com.pajato.isaac.core.client.Client
import com.pajato.isaac.core.client.defaultServerSpecPath
import kotlin.test.Test
import kotlin.test.assertEquals

class ClientSupportUnitTest : InstrumenterTestProfiler() {

    @Test fun `When using all defaults to get a client using getClient(), verify correct behavior`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val testServerSpec: ServerSpec = classLoader.getServerSpec(fwcdKLS, "kotlin_gradle", "basics")
        val client: Client = getClient(testServerSpec)
        assertEquals(defaultServerSpecPath, client.registryPath, getDiagnostic("default registry path"))
        assertEquals("", client.cachePath, getDiagnostic("default cache path"))
    }
}
