package com.pajato.edgar.collector

import com.pajato.edgar.collector.DataCollector.addExecutableSnippet
import com.pajato.edgar.instrumenter.getDiagnostic
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SymbolKind
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DataCollectorUnitTest {

    @Test fun `When recording a coverage event, verify correct result`() {
        fun assertDataCollectorCount(expected: Int, id: Int) {
            assertTrue(DataCollector.dataMap.containsKey(id), getDiagnostic("data map key"))
            assertEquals(expected, DataCollector.dataMap[id]!!.executionCount, getDiagnostic("data map value"))
        }

        fun assertDataCollectorSizes(expected: Int) =
            assertEquals(expected, DataCollector.dataMap.size, getDiagnostic("data map size"))

        fun assertIsInstrumented() {
            val id = DataCollector.getNextId()
            val range = Range(Position(0, 0), Position(0, 0))
            val symbol = DocumentSymbol("testSymbol", SymbolKind.Property, range = range, selectionRange = range)
            addExecutableSnippet(id, symbol)
            assertEquals(0, id, getDiagnostic("snippet index"))
            assertDataCollectorSizes(1)
            assertDataCollectorCount(0, id)
        }

        fun assertCoverageCount(id: Int, count: Int) = assertDataCollectorCount(count, id)

        fun assertCollectorIsInitialized() {
            DataCollector.clear()
            assertDataCollectorSizes(0)
        }

        val id = 0
        assertCollectorIsInitialized()
        assertIsInstrumented()
        assertCoverageCount(id, 0)
        DataCollector.record(id) { println("Hello World") }
        assertCoverageCount(id, 1)
        DataCollector.record(1) { println("Hello World") }
    }
}
