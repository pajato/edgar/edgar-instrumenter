@file:Suppress("unused", "PackageDirectoryMismatch", "PackageDirectoryMismatch")

package samples

var varWithMultipleFunctionInitializer: String = getZ(getZ(getZ()))

fun getZ(arg: String = "abc"): String {
    return arg
}
