#!/bin/sh

echo Running KLS for version: "$1" with test: "$2"
version=$1
export PATH="$PATH":"$PWD"/kls-"$version"/bin
kotlin-language-server
