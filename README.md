# The Edgar coverage tool source code instrumenter

This project provides a means to instrument source code from a project in order to subsequently collect coverage data.

## Use cases

tbc

## Structure

tbc

## Building

Install the deployable (edgar-instrumenter-<version>.jar) to the local maven repository using the Gradle task: *publishToMavenLocal* where it will be accessed by Edgar subprojects which depend on the core.
