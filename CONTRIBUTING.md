wip:

For a PR to be accepted (merged) the following will be evaluated and considered:

Is the source code clean? Is it spotless? Does it have a bad smell?
- Are there spurious and unnecessary comments?
- Are functions of length ~4 lines long?
- Is block structure employed (see Structure and Interpretation fo Computer Programs - SICP, by Abelson & Sussman, MIT Press)
- Does the source code scream "CODE COVERAGE TOOL"?
- Do names reveal intent?

Is the test coverage support nearly 100%?

Is the code coverage support 100%?

Does the PR address an issue (bugs are issues)?
